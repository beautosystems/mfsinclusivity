package com.mfs.client.inclusivity.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dto.StatusResponseDto;
import com.mfs.client.inclusivity.service.CustomerStatusService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class CustomerStatusServiceTest {

	@Autowired
	CustomerStatusService customerStatusService;

	// Function to test success
	@Ignore
	@Test
	public void CustomerStatusSuccessTest() {

		StatusResponseDto response = customerStatusService.getStatus("00250002501234");
		Assert.assertEquals("MFS Insurance", response.getService_name());
	}

	// Function to test Fail
	@Ignore
	@Test
	public void CustomerStatusFailTest() {

		StatusResponseDto response = customerStatusService.getStatus("");
		Assert.assertEquals(203, response.getErrorCode());

	}

	// Function to test NotNull
	@Ignore
	@Test
	public void CustomerStatusNotNullTest() {

		StatusResponseDto response = customerStatusService.getStatus("00250002501234");
		Assert.assertNotNull(response);

	}

}
