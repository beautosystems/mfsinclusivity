package com.mfs.client.inclusivity.dao.test;

import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class InclusivitySystemConfigTest {

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;
	
	@Ignore
	@Test
	public void inclusivitySystemConfigDaoTest() {
		
		Map<String,String> map = inclusivitySystemConfigDao.getConfigDetailsMap();
		Assert.assertNotNull(map);
		System.out.println(map);

		
	}
	
}
