package com.mfs.client.inclusivity.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.service.AuthorizationService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthorizationServiceTest {

	@Autowired
	AuthorizationService authorizationService;
	
	
	    // function to test success
		@Ignore
		@Test
		public void AuthorizationSuccessTest() {

			AuthorizationResponseDto response = authorizationService.authorizationService();
			Assert.assertEquals("bearer", response.getToken_type());
		}

		 // function to test Not Null
		@Ignore
		@Test
		public void AuthorizationNotNullTest() {

			AuthorizationResponseDto response = authorizationService.authorizationService();
			Assert.assertEquals("MFSClient", response.getUser_client_name());
		}
			
}
