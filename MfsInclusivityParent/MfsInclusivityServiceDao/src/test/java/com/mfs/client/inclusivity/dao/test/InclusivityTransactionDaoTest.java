package com.mfs.client.inclusivity.dao.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.exception.DaoException;
import com.mfs.client.inclusivity.models.InclusivityPartnerCountry;
import com.mfs.client.inclusivity.models.InclusivityTransactionData;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class InclusivityTransactionDaoTest {

	@Autowired
	TransactionDao transactionDao;

	// Check for partner details.
	@Ignore
	@Test
	public void getPartnerDetail() throws DaoException {
		InclusivityPartnerCountry inclusivityPartnerCountry = new InclusivityPartnerCountry();
		inclusivityPartnerCountry = transactionDao.getPartnerDetail();
		Assert.assertNotNull(inclusivityPartnerCountry);
	}

	// Check for transaction by MFStransId
	@Ignore
	@Test
	public void transactionBytransactionId() throws DaoException {

		InclusivityTransactionData inclusivityTransactionLog = new InclusivityTransactionData();
		inclusivityTransactionLog = transactionDao.getTransactionByMfstransId("1156449655254");
		Assert.assertNotNull(inclusivityTransactionLog);
	}

	// Check for transaction by MFStransId null test
	@Ignore
	@Test
	public void transactionBytransactionIdEmpty() throws DaoException {

		InclusivityTransactionData inclusivityTransactionData = new InclusivityTransactionData();
		inclusivityTransactionData = transactionDao.getTransactionByMfstransId("123");
		Assert.assertNull(inclusivityTransactionData);
	}
}
