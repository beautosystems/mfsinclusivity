package com.mfs.client.inclusivity.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.service.OptOutService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class OptOutTest {

	@Autowired
	OptOutService optOutService;

	// function to test success
	@Ignore
	@Test
	public void optOutServiceSuccessTest() {

		ResponseStatus response = optOutService.optOut("00250002501234");
		Assert.assertEquals("202", response.getStatusCode());
	}

	// function to test  Fail
	@Ignore
	@Test
	public void optOutServiceFailTest() {

		ResponseStatus response = optOutService.optOut("250786131334");
		Assert.assertEquals("404", response.getStatusCode());
		System.out.println(response);
	}

	// Function to test NotNull
	@Ignore
	@Test
	public void optOutServiceNotNullTest() {

		ResponseStatus response = optOutService.optOut("00250002501234");
		Assert.assertNotNull(response);
		System.out.println(response);
	}
}
