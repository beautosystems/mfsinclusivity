package com.mfs.client.inclusivity.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dto.StatusResponseDto;
import com.mfs.client.inclusivity.service.RemittanceStatusService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class RemittanceStatusServiceTest {

	@Autowired
	RemittanceStatusService remittanceStatusService;

	// Function to test success
	@Ignore
	@Test
	public void RemittanceStatusSuccessTest() {

		StatusResponseDto response = remittanceStatusService.remittanceStatus("00250002501234");
		Assert.assertEquals("00250002501234", response.getMsisdn());
		System.out.println(response);
	}

	// Function to test Fail
	@Ignore
	@Test
	public void RemittanceStatusFailTest() {

		StatusResponseDto response = remittanceStatusService.remittanceStatus("");
		Assert.assertEquals(203, response.getErrorCode());
		System.out.println(response);

	}

	// Function to test NotNull
	@Ignore
	@Test
	public void RemittanceStatusNotNullTest() {

		StatusResponseDto response = remittanceStatusService.remittanceStatus("00250002501234");
		Assert.assertNotNull(response);
		System.out.println(response);

	}

}
