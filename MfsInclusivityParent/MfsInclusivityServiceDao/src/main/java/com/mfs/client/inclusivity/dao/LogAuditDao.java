package com.mfs.client.inclusivity.dao;

import com.mfs.client.inclusivity.models.InclusivityTransactionAudit;

public interface LogAuditDao {

	public long logAuditDetails(InclusivityTransactionAudit audit);

}
