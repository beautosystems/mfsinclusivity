package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_partner_mapping")
public class InclusivityPartnerMapping {

	@Id
	@GeneratedValue
	@Column(name = "partner_mapping_id")
	private int partnerMappingId;

	@Column(name = "partner_name", length = 50)
	private String partnerName;

	@Column(name = "country", length = 20)
	private String country;

	@Column(name = "currency", length = 20)
	private String currency;

	@Column(name = "mfs_corporate_code", length = 20)
	private String mfsCorporateCode;

	@Column(name = "mfs_password", length = 20)
	private String mfsPassword;

	@Column(name = "icl_url", length = 50)
	private String iclUrl;

	@Column(name = "icl_guid", length = 20)
	private String iclGuid;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getPartnerMappingId() {
		return partnerMappingId;
	}

	public void setPartnerMappingId(int partnerMappingId) {
		this.partnerMappingId = partnerMappingId;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getMfsCorporateCode() {
		return mfsCorporateCode;
	}

	public void setMfsCorporateCode(String mfsCorporateCode) {
		this.mfsCorporateCode = mfsCorporateCode;
	}

	public String getMfsPassword() {
		return mfsPassword;
	}

	public void setMfsPassword(String mfsPassword) {
		this.mfsPassword = mfsPassword;
	}

	public String getIclUrl() {
		return iclUrl;
	}

	public void setIclUrl(String iclUrl) {
		this.iclUrl = iclUrl;
	}

	public String getIclGuid() {
		return iclGuid;
	}

	public void setIclGuid(String iclGuid) {
		this.iclGuid = iclGuid;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "InclusivityPartnerMapping [partnerMappingId=" + partnerMappingId + ", partnerName=" + partnerName
				+ ", country=" + country + ", currency=" + currency + ", mfsCorporateCode=" + mfsCorporateCode
				+ ", mfsPassword=" + mfsPassword + ", iclUrl=" + iclUrl + ", iclGuid=" + iclGuid + ", dateLogged="
				+ dateLogged + "]";

	}
}