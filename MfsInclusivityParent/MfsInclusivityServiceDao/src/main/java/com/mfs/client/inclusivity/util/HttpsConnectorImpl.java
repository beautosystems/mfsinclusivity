/**
 * 
 */
package com.mfs.client.inclusivity.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.log4j.Logger;

import com.mfs.client.inclusivity.service.impl.TransactionDataServiceImpl;

public class HttpsConnectorImpl {

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final int CHECK_RESPONSE_CODE201 = 200;
	
	private static final Logger LOGGER = Logger.getLogger(HttpsConnectorImpl.class);


	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException, Exception {
		SSLContext sslContext = null;
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();

		try {
			sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			throw new Exception(e);
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Read timeout Changed to 60 sec
		httpsConnection.setReadTimeout(60000);

		// Send post request
		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {
			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

		wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();
		
		LOGGER.info("==>response code " + responseCode);


		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE201) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else if (responseCode == 202) {

			connectionResponse.setRespCode(responseCode);
		} else {
			connectionResponse.setRespCode(responseCode);
		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}

		if (output != null) {
			connectionResponse.setResponseData(output);
			connectionResponse.setRespCode(responseCode);

		}

		return connectionResponse;
	}

	/**
	 * @param httpsResponse
	 * @return
	 * @throws IOException
	 */
	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	/**
	 * Set Header Properties To HTTPS Connection
	 * 
	 * @param httpsConnectionRequest
	 * @param connectionData
	 * @param httpsConnection
	 */
	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpsConnection.setRequestProperty("Content-Length", "0");
		}

	}

	/**
	 * Set headers that are required by calling app
	 * 
	 * @param headers
	 * @param httpsConnection
	 */
	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

}
