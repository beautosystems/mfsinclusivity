package com.mfs.client.inclusivity.dto;

public class UpsellResponseDto {

	private int night_cover;
	private int minimum_nights;
	private int maximum_nights;
	private int fixed_benefit_amount;
	private int eligibility_threshold;
	private int waiting_period_days;
	private int minimum_age;
	private int maximum_age;
	private boolean cash_back_enabled;

	public int getNight_cover() {
		return night_cover;
	}

	public void setNight_cover(int night_cover) {
		this.night_cover = night_cover;
	}

	public int getMinimum_nights() {
		return minimum_nights;
	}

	public void setMinimum_nights(int minimum_nights) {
		this.minimum_nights = minimum_nights;
	}

	public int getMaximum_nights() {
		return maximum_nights;
	}

	public void setMaximum_nights(int maximum_nights) {
		this.maximum_nights = maximum_nights;
	}

	public int getFixed_benefit_amount() {
		return fixed_benefit_amount;
	}

	public void setFixed_benefit_amount(int fixed_benefit_amount) {
		this.fixed_benefit_amount = fixed_benefit_amount;
	}

	public int getEligibility_threshold() {
		return eligibility_threshold;
	}

	public void setEligibility_threshold(int eligibility_threshold) {
		this.eligibility_threshold = eligibility_threshold;
	}

	public int getWaiting_period_days() {
		return waiting_period_days;
	}

	public void setWaiting_period_days(int waiting_period_days) {
		this.waiting_period_days = waiting_period_days;
	}

	public int getMinimum_age() {
		return minimum_age;
	}

	public void setMinimum_age(int minimum_age) {
		this.minimum_age = minimum_age;
	}

	public int getMaximum_age() {
		return maximum_age;
	}

	public void setMaximum_age(int maximum_age) {
		this.maximum_age = maximum_age;
	}

	public boolean isCash_back_enabled() {
		return cash_back_enabled;
	}

	public void setCash_back_enabled(boolean cash_back_enabled) {
		this.cash_back_enabled = cash_back_enabled;
	}

	@Override
	public String toString() {
		return "UpsellResponseDto [night_cover=" + night_cover + ", minimum_nights=" + minimum_nights
				+ ", maximum_nights=" + maximum_nights + ", fixed_benefit_amount=" + fixed_benefit_amount
				+ ", eligibility_threshold=" + eligibility_threshold + ", waiting_period_days=" + waiting_period_days
				+ ", minimum_age=" + minimum_age + ", maximum_age=" + maximum_age + ", cash_back_enabled="
				+ cash_back_enabled + "]";
	}

}
