package com.mfs.client.inclusivity.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.service.OptOutService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.CommonValidations;
import com.mfs.client.inclusivity.util.ExpiredToken;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;
import com.mfs.client.inclusivity.util.InclusivityCodes;

@Service("OptOutService")
public class OptOutServiceImpl implements OptOutService {

	private static final Logger LOGGER = Logger.getLogger(OptOutServiceImpl.class);

	@Resource(name = "responseC")
	private Properties responseCodes;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	@Autowired
	AuthorizationService authorizationService;

	public ResponseStatus optOut(String msisdn) {

		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		Map<String, String> configMap = null;
		String token = null;
		String inclusivityResponse = null;
		AuthorizationResponseDto authResponse = null;
		ResponseStatus responseStatus = new ResponseStatus();

		try {

			// getting access token data from db
			InclusivityAuthorizationModel inclusivityAuthorizationModel = transactionDao.getTokenData();

			// checking null value and expiration for token
			if (null != inclusivityAuthorizationModel && ExpiredToken.isTokenExpire(inclusivityAuthorizationModel)) {

				// getting access token from db
				token = inclusivityAuthorizationModel.getAccessToken();
			} else {
				authResponse = new AuthorizationResponseDto();

				// calling auth service
				authResponse = authorizationService.authorizationService();
				// checking null response
				if (null != authResponse) {

					// logging auth response in db
					authorizationService.logResponse(authResponse);
					token = authResponse.getAccess_token();
				}

				else {
					LOGGER.error("==>Null auth response " + authResponse);

				}

			}

			// getting data from config table
			configMap = inclusivitySystemConfigDao.getConfigDetailsMap();
			if (configMap == null) {
				throw new Exception(" No Config Details in DB");
			}

			String partnerGuId = configMap.get(CommonConstant.PARTNER_GUID);
			connectionRequest.setHttpmethodName(CommonConstant.PUT);
			connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			connectionRequest.setServiceUrl(
					configMap.get(CommonConstant.INCLUSIVITY_URL) + configMap.get(CommonConstant.REMITANCE_ENDPOINT)
							+ configMap.get(CommonConstant.OPT_OUT_ENDPOINT) + "/" + msisdn + "?" + "access_token="
							+ inclusivityAuthorizationModel.getAccessToken() + "&" + "partner=" + partnerGuId);
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			connectionRequest.setHeaders(headerMap);

			// calling inclusitivity
			inclusivityResponse = CallServices.getResponseFromService(connectionRequest, null);

			LOGGER.info("==>OptOut Service response " + inclusivityResponse);

			// checking null response for inclusivity
			if (CommonValidations.isStringNotEmptyAndNotNull(inclusivityResponse)) {

				if (inclusivityResponse.equals(CommonConstant.ICL_SUCCESS_CODE)) {

					responseStatus.setStatusCode(inclusivityResponse);
					responseStatus.setStatusMessage(InclusivityCodes.S202.getMessage());

				} else if (inclusivityResponse.equals(CommonConstant.ICL_FAIL_CODE)
						|| (inclusivityResponse.charAt(0)) == '5') {

					if (inclusivityResponse.charAt(0) == CommonConstant.FAIL_ERROR_CHECK) {

						responseStatus.setStatusCode(inclusivityResponse);
						responseStatus.setStatusMessage(CommonConstant.RESPONSEMESSAGE2);
					} else {

						responseStatus.setStatusCode(CommonConstant.ICL_FAIL_CODE);
						responseStatus.setStatusMessage(CommonConstant.RESPONSEMESSAGE1);
					}

				}  else if(inclusivityResponse.equals(CommonConstant.ER404)) {

					responseStatus.setStatusCode(CommonConstant.ER404);
					responseStatus.setStatusMessage(CommonConstant.RESPONSEMESSAGE5);
				}else {
					
					responseStatus.setStatusCode(inclusivityResponse);
					responseStatus.setStatusMessage(CommonConstant.INCLUSIVITY_ERROR);
				}
			} else {

				LOGGER.error("==>null response from inclusivity " + inclusivityResponse);
			}
		} catch (Exception e) {

			LOGGER.error("==>Exception in OptOutServiceImpl" + e);
			
			responseStatus.setStatusCode(CommonConstant.EXCEPTION);
			responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.EXCEPTION));
		}

		return responseStatus;
	}
}
