package com.mfs.client.inclusivity.dto;

public class CurbiRequestDto {

	private String partnerCode;

	private String startDate;

	private String endDate;

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "CurbiRequestDto [partnerCode=" + partnerCode + ", startDate=" + startDate + ", endDate=" + endDate
				+ "]";
	}

}
