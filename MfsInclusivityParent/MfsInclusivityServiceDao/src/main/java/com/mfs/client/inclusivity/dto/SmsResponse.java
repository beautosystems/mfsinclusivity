package com.mfs.client.inclusivity.dto;

import java.util.List;

public class SmsResponse {

	private List<Message> messages;

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "SmsResponse [messages=" + messages + "]";
	}

}
