package com.mfs.client.inclusivity.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TransactionDataDto {
	private String partner_guid;
	private String transaction_id;
	private String amount_in_cents;
	private String currency;
	private String msisdn;
	private String national_id;
	private String full_name;
	private String date_of_birth;

	public String getPartner_guid() {
		return partner_guid;
	}

	public void setPartner_guid(String partner_guid) {
		this.partner_guid = partner_guid;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAmount_in_cents() {
		return amount_in_cents;
	}

	public void setAmount_in_cents(String amount_in_cents) {
		this.amount_in_cents = amount_in_cents;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getNational_id() {
		return national_id;
	}

	public void setNational_id(String national_id) {
		this.national_id = national_id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	@Override
	public String toString() {
		return "TransactionDataDto [partner_guid=" + partner_guid + ", transaction_id=" + transaction_id
				+ ", amount_in_cents=" + amount_in_cents + ", currency=" + currency + ", msisdn=" + msisdn
				+ ", national_id=" + national_id + ", full_name=" + full_name + ", date_of_birth=" + date_of_birth
				+ "]";
	}

}
