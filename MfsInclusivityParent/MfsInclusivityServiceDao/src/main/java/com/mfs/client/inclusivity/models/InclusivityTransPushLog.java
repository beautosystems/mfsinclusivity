package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_trans_push_log")
public class InclusivityTransPushLog {

	@Id
	@GeneratedValue
	@Column(name = "trans_push_log_id")
	private int transPushLogId;

	@Column(name = "date_time_pushed")
	private Date dateTimePushed;

	@Column(name = "partner_code", length = 50)
	private String partnerCode;

	@Column(name = "tot_record_pushed")
	private int totRecordPushed;

	@Column(name = "status", length = 50)
	private String status;

	public int getTransPushLogId() {
		return transPushLogId;
	}

	public void setTransPushLogId(int transPushLogId) {
		this.transPushLogId = transPushLogId;
	}

	public Date getDateTimePushed() {
		return dateTimePushed;
	}

	public void setDateTimePushed(Date dateTimePushed) {
		this.dateTimePushed = dateTimePushed;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public int getTotRecordPushed() {
		return totRecordPushed;
	}

	public void setTotRecordPushed(int totRecordPushed) {
		this.totRecordPushed = totRecordPushed;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "InclusivityTransPushLog [transPushLogId=" + transPushLogId + ", dateTimePushed=" + dateTimePushed
				+ ", partnerCode=" + partnerCode + ", totRecordPushed=" + totRecordPushed + ", status=" + status + "]";
	}

}