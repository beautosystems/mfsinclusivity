package com.mfs.client.inclusivity.dto;

public class Message {

	private String to;
	private String messageId;
	private String smsCount;
	private Status status;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getSmsCount() {
		return smsCount;
	}

	public void setSmsCount(String smsCount) {
		this.smsCount = smsCount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Message [to=" + to + ", messageId=" + messageId + ", smsCount=" + smsCount + ", status=" + status + "]";
	}

}
