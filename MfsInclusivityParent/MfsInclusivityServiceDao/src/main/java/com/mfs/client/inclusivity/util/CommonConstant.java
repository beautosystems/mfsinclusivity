package com.mfs.client.inclusivity.util;

public class CommonConstant {

	public static final String BASE_URL = "base_url";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String TEXT_XML = "text/xml";

	public static final String PORT = "port";
	public static final String HTTP_METHOD = "httpmethod";
	public static final String ISHTTPS = "ishttps";

	public static final String INCLUSIVITYEXCEPTION = "204";
	public static final String INCLUSIVITYEXCEPTIONINUPDATE = "205";
	public static final String CURBI_ENDPOINT = "curbi";
	public static final String PARTNER_GUID = "partner_guid";

	public static final String BASE_URL1 = "base_url1";
	public static final String REMITANCE_ENDPOINT = "remitance_endpoint";
	public static final String AUTH_ENDPOINT = "auth_endpoint";
	public static final String ACCESS_TOKEN = "?access_token=";
	public static final String MFSEXCEPTION = "201";
	public static final String PARTNERDETAILEXCEPTION = "209";
	public static final String TRANSACTIONLOGEXCEPTION = "210";
	public static final String INCLUSIVITYRESPONSEEXCEPTION = "211";
	public static final String CURBIEXCEPTION = "212";
	public static final String NATIONAL_ID = "national_id";
	public static final String Transaction_Retrieve = "transaction_retrieve";
	public static final String BASE_URL2 = "base_url2";
	public static final String INCLUSIVITY_URL = "inclusivity_url";

	public static final String SMS_USERNAME = "username";
	public static final String SMS_PASSWORD = "password";

	public static final String AUTH_USERNAME = "auth_username";
	public static final String AUTH_PASSWORD = "auth_password";
	public static final String AUTH_URL = "auth_url";

	public static final String SMS_URL = "sms_url";
	public static final String SENDER = "sender";
	public static final String SUCCESS_CODE = "200";
	public static final String ICL_SUCCESS_CODE = "202";
	public static final String ICL_FAIL_CODE = "400";
	public static final String FAIL_CODE = "401";
	public static final String CURBI_DATA = "212";
	public static final String POST = "POST";
	public static final String PENDING = "303";
	public static final String TOKEN_EXPIRY_CODE = "401";
	public static final String EXCEPTION = "203";

	public static final String RETRY_COUNT = "retry_count";

	public static final String RESPONSEMESSAGE1 = "Active customer already has opted out";
	public static final String RESPONSEMESSAGE2 = "The server is currently unhealthy, retrying the call after a few minutes might be needed.";
	public static final String RESPONSEMESSAGE3 = "MFS system error";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER = "Bearer ";
	public static final String BASIC = "Basic ";
	public static final Integer FAIL_ERROR_CHECK = 5;
	public static final String SWITCH = "switch";
	public static final String ON = "on";
	public static final String DUMMY_SMS_URL = "dummy_sms_url";
	public static final String OPT_IN = "opt_in";
	public static final String PARTNER = "&partner=";
	public static final String PUT = "PUT";
	public static final String OPT_OUT_ENDPOINT = "opt_out_endpoint";
	public static final String CLAIM_ENDPOINT = "claim_endpoint";
	public static final String STATUS = "status_endpoint";
	public static final String LOYALTY_ELIGIBILITY_INCLUDED = "&loyalty_eligibility_included=";
	public static final String CUSTOMERS = "customer_endpoint";
	public static final String PARTNERS = "?partner=";
	public static final String ACCESS_TOKENS = "&access_token=";
	public static final String REMITTANCE_STATUS = "remittance status";
	public static final String CUSTOMER_STATUS = "customer status";
	public static final String GET = "GET";
	public static final String ER400 = "400";
	public static final String ER404 = "404";
	public static final String RESPONSEMESSAGE4 = "Customer can't have a valid claim because there was never a policy";
	public static final String RESPONSEMESSAGE5 = "Customer msisdn could not be found. Please register first.";
	public static final String INCLUSIVITY_ERROR = "Inclusivity system error";
	public static final String RESPONSEMESSAGE6 = "Active customer already has opted in";

}