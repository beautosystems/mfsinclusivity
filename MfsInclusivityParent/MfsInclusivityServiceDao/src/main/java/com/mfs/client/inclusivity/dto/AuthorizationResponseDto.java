package com.mfs.client.inclusivity.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AuthorizationResponseDto {

	private String access_token;
	private String token_type;
	private String expires_in;
	private String scope;
	private String user_client_name;
	private String user_principle;
	private String code;
	private String message;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getUser_client_name() {
		return user_client_name;
	}

	public void setUser_client_name(String user_client_name) {
		this.user_client_name = user_client_name;
	}

	public String getUser_principle() {
		return user_principle;
	}

	public void setUser_principle(String user_principle) {
		this.user_principle = user_principle;
	}

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "AuthorizationResponseDto [access_token=" + access_token + ", token_type=" + token_type + ", expires_in="
				+ expires_in + ", scope=" + scope + ", user_client_name=" + user_client_name + ", user_principle="
				+ user_principle + ", code=" + code + ", message=" + message + "]";
	}

}
