package com.mfs.client.inclusivity.service.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.Message;
import com.mfs.client.inclusivity.dto.SendSms;
import com.mfs.client.inclusivity.dto.SmsResponse;
import com.mfs.client.inclusivity.dto.Status;
import com.mfs.client.inclusivity.models.InclusivitySMSLogQueue;
import com.mfs.client.inclusivity.models.InclusivitySMSLogSuccess;
import com.mfs.client.inclusivity.service.NotifyService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.CommonValidations;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;

@Service()
public class NotifyServiceImpl implements NotifyService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	InclusivitySystemConfigDao inclusivityConfig;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(NotifyServiceImpl.class);

	public List<SmsResponse> sendSMS() {

		Map<String, String> configMap = null;
		String smsResponse = null;
		Message response = new Message();
		SmsResponse smsDataDto = null;
		List<SmsResponse> smsDtoList = new ArrayList<SmsResponse>();
		List<Message> list = new ArrayList<Message>();
		try {

			List<InclusivitySMSLogQueue> inclusivitySMSLogQueue = transactionDao.getSmsRecord();
			if (null != inclusivitySMSLogQueue & !(inclusivitySMSLogQueue.isEmpty())) {

				SendSms sendSms = new SendSms();
				HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
				Gson gson = new Gson();
				configMap = inclusivityConfig.getConfigDetailsMap();
				// String from = configMap.get(CommonConstant.SENDER);
				sendSms.setFrom(configMap.get(CommonConstant.SENDER));

				StringBuilder auth = new StringBuilder();
				auth.append(configMap.get(CommonConstant.SMS_USERNAME));
				auth.append(":");
				auth.append(configMap.get(CommonConstant.SMS_PASSWORD));

				byte[] authEncBytes = Base64.encodeBase64(auth.toString().getBytes());
				String basicAuth = new String(authEncBytes);

				basicAuth = CommonConstant.BASIC + basicAuth;

				connectionRequest.setHttpmethodName(CommonConstant.POST);
				if (CommonValidations.isStringNotEmptyAndNotNull(configMap.get(CommonConstant.PORT))) {
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				}
				if (configMap.get(CommonConstant.SWITCH).equalsIgnoreCase(CommonConstant.ON)) {

					connectionRequest.setServiceUrl(configMap.get(CommonConstant.SMS_URL));

				} else {
					connectionRequest.setServiceUrl(configMap.get(CommonConstant.DUMMY_SMS_URL));
				}
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.AUTHORIZATION, basicAuth);

				connectionRequest.setHeaders(headerMap);

				for (InclusivitySMSLogQueue inclusivitySMSLogQueue1 : inclusivitySMSLogQueue) {

					sendSms.setText(inclusivitySMSLogQueue1.getMessage());
					sendSms.setTo(inclusivitySMSLogQueue1.getMsisdn());

					smsResponse = CallServices.getResponseFromService(connectionRequest, gson.toJson(sendSms));

					if (CommonValidations.isStringNotEmptyAndNotNull(smsResponse)) {

						Type collectionType = new TypeToken<SmsResponse>() {
						}.getType();

						smsDataDto = gson.fromJson(smsResponse, collectionType);
						logSuccessData(smsDataDto, responseCodes.getProperty(CommonConstant.SUCCESS_CODE), null);
						transactionDao.delete(inclusivitySMSLogQueue1);
						smsDtoList.add(smsDataDto);

					} else {

						smsDataDto = new SmsResponse();
						response = new Message();
						Status status = new Status();
						logSuccessData(null, responseCodes.getProperty(CommonConstant.FAIL_CODE),
								inclusivitySMSLogQueue1.getMsisdn());
						transactionDao.delete(inclusivitySMSLogQueue1);
						status.setGroupName(responseCodes.getProperty(CommonConstant.FAIL_CODE));
						status.setDescription("Message send failed");
						response.setStatus(status);
						response.setTo(inclusivitySMSLogQueue1.getMsisdn());
						list.add(response);
						smsDataDto.setMessages(list);
						smsDtoList.add(smsDataDto);

						LOGGER.error("==>sending sms fail" + smsResponse);
					}

				}

			} else {
				LOGGER.error("==>no record in queue for sms" + inclusivitySMSLogQueue);
			}

		} catch (Exception e) {
			LOGGER.error("==>exception while processing smsapi" + e.getMessage());
		}
		return smsDtoList;

	}

	private void logSuccessData(SmsResponse smsResponse, String status, String msisdn) {

		InclusivitySMSLogSuccess inclusivitySMSLogSuccess = new InclusivitySMSLogSuccess();
		if (null != smsResponse) {
			List<Message> message = smsResponse.getMessages();

			if (!(message.isEmpty())) {
				for (Message msg : message) {
					inclusivitySMSLogSuccess.setMessageId(msg.getMessageId());
					inclusivitySMSLogSuccess.setMsisdn(msg.getTo());
					inclusivitySMSLogSuccess.setDescription(msg.getStatus().getDescription());
					inclusivitySMSLogSuccess.setGroupId(msg.getStatus().getGroupId());
					inclusivitySMSLogSuccess.setGroupName(msg.getStatus().getGroupName());
					inclusivitySMSLogSuccess.setId(msg.getStatus().getId());
					inclusivitySMSLogSuccess.setName(msg.getStatus().getName());
					inclusivitySMSLogSuccess.setSmsCount(Integer.parseInt(msg.getSmsCount()));
					inclusivitySMSLogSuccess.setStatus(status);
				}
				inclusivitySMSLogSuccess.setDateLogged(new Date());

			}
		} else {
			inclusivitySMSLogSuccess.setStatus(status);
			inclusivitySMSLogSuccess.setDateLogged(new Date());
			inclusivitySMSLogSuccess.setMsisdn(msisdn);
		}
		transactionDao.save(inclusivitySMSLogSuccess);

	}

}