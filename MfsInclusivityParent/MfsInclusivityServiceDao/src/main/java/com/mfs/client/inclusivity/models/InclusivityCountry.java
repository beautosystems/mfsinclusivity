package com.mfs.client.inclusivity.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "icl_country")
public class InclusivityCountry {

	@Id
	@GeneratedValue
	@Column(name = "country_id")
	private int countryId;

	@Column(name = "country_name", length = 20)
	private String countryName;

	@Column(name = "country_code", length = 20)
	private String countryCode;

	@OneToMany(mappedBy = "inclusivityCountry", cascade = CascadeType.ALL)
	private List<InclusivityPartnerCountry> inclusivityPartnerCountry;

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<InclusivityPartnerCountry> getInclusivityPartnerCountry() {
		return inclusivityPartnerCountry;
	}

	public void setInclusivityPartnerCountry(List<InclusivityPartnerCountry> inclusivityPartnerCountry) {
		this.inclusivityPartnerCountry = inclusivityPartnerCountry;
	}

	@Override
	public String toString() {
		return "InclusivityCountry [countryId=" + countryId + ", countryName=" + countryName + ", countryCode="
				+ countryCode + ", inclusivityPartnerCountry=" + inclusivityPartnerCountry + "]";
	}

}