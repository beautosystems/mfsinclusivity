package com.mfs.client.inclusivity.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "icl_push_job_track")
public class InclusivityPushJobTrack {

	@Id
	@GeneratedValue
	@Column(name = "push_job_track_Id")
	private int pushJobTrackId;

	@Column(name = "last_success_date_time", length = 50)
	private Timestamp lastSuccessDateTime;

	@Column(name = "cron_status", length = 20)
	private String cronStatus;

	@OneToOne
	@JoinColumn(name = "partner_id")
	private InclusivityPartnerCountry inclusivityPartnerCountry;

	public int getPushJobTrackId() {
		return pushJobTrackId;
	}

	public void setPushJobTrackId(int pushJobTrackId) {
		this.pushJobTrackId = pushJobTrackId;
	}

	public Timestamp getLastSuccessDateTime() {
		return lastSuccessDateTime;
	}

	public void setLastSuccessDateTime(Timestamp lastSuccessDateTime) {
		this.lastSuccessDateTime = lastSuccessDateTime;
	}

	public String getCronStatus() {
		return cronStatus;
	}

	public void setCronStatus(String cronStatus) {
		this.cronStatus = cronStatus;
	}

	public InclusivityPartnerCountry getInclusivityPartnerCountry() {
		return inclusivityPartnerCountry;
	}

	public void setInclusivityPartnerCountry(InclusivityPartnerCountry inclusivityPartnerCountry) {
		this.inclusivityPartnerCountry = inclusivityPartnerCountry;
	}

	@Override
	public String toString() {
		return "InclusivityPushJobTrack [pushJobTrackId=" + pushJobTrackId + ", lastSuccessDateTime="
				+ lastSuccessDateTime + ", cronStatus=" + cronStatus + ", inclusivityPartnerCountry="
				+ inclusivityPartnerCountry + "]";
	}

}