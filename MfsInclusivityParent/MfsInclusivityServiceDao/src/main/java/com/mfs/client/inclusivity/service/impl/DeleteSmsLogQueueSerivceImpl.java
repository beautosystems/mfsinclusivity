package com.mfs.client.inclusivity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.models.InclusivitySMSLogQueue;
import com.mfs.client.inclusivity.models.InclusivitySMSLogSuccess;
import com.mfs.client.inclusivity.service.DeleteSmsLogQueueSerivce;

@Service("DeleteSmsLogQueueSerivce")
public class DeleteSmsLogQueueSerivceImpl implements DeleteSmsLogQueueSerivce {

	@Autowired
	TransactionDao transactionDao;

	public void deleteLogQueueData() {

		List<InclusivitySMSLogSuccess> inclusivitySMSLogSuccess = transactionDao.getSmsLogSuccessData();

		if (inclusivitySMSLogSuccess != null) {

			for (InclusivitySMSLogSuccess inclusivitySMSLogSuccess1 : inclusivitySMSLogSuccess) {

				String msisdn = inclusivitySMSLogSuccess1.getMsisdn();

				InclusivitySMSLogQueue inclusivitySMSLogQueue = transactionDao.deleteLogQueueDataByMsisdn(msisdn);

			}
		}

	}

}
