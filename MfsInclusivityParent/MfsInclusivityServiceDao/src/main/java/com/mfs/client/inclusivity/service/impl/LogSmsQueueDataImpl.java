package com.mfs.client.inclusivity.service.impl;

import java.util.Date;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.dto.SMSApiRequestDto;
import com.mfs.client.inclusivity.models.InclusivitySMSLogQueue;
import com.mfs.client.inclusivity.service.LogSmsQueueData;
import com.mfs.client.inclusivity.util.CommonConstant;

@Service
public class LogSmsQueueDataImpl implements LogSmsQueueData {

	@Autowired
	TransactionDao transactionDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public ResponseStatus logSmsQueueData(SMSApiRequestDto requestDto) {

		ResponseStatus responseStatus = new ResponseStatus();
		InclusivitySMSLogQueue inclusivitySMSLogQueue = new InclusivitySMSLogQueue();
		inclusivitySMSLogQueue.setMsisdn(requestDto.getMsisdn());
		inclusivitySMSLogQueue.setMessage(requestDto.getMessage());
		inclusivitySMSLogQueue.setDateLogged(new Date());
		boolean result = transactionDao.save(inclusivitySMSLogQueue);
		if (result) {
			responseStatus.setStatusCode(CommonConstant.SUCCESS_CODE);
			responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.SUCCESS_CODE));
		} else {
			responseStatus.setStatusCode(CommonConstant.FAIL_CODE);
			responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.FAIL_CODE));
		}
		return responseStatus;

	}
}
