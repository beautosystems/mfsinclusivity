package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;

public interface AuthorizationService {

	public AuthorizationResponseDto authorizationService();
	
	public void logResponse(AuthorizationResponseDto authorizationResponse);
	
}
