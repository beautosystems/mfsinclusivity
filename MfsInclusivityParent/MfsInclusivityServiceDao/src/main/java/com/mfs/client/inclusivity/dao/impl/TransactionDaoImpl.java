package com.mfs.client.inclusivity.dao.impl;

import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.models.InclusivityPartnerCountry;
import com.mfs.client.inclusivity.models.InclusivityPushJobTrack;
import com.mfs.client.inclusivity.models.InclusivitySMSLogQueue;
import com.mfs.client.inclusivity.models.InclusivitySMSLogSuccess;
import com.mfs.client.inclusivity.models.InclusivityTransactionData;

@EnableTransactionManagement
@Repository("TransactionDao")
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Resource(name = "responseC")
	private Properties responseCodes;

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	@Transactional
	public InclusivityPartnerCountry getPartnerDetail() {

		InclusivityPartnerCountry inclusivityPartnerDetail = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivityPartnerDetail = (InclusivityPartnerCountry) session.createQuery("From InclusivityPartnerCountry")
					.uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in getPartnerDetail" + e);
		}
		return inclusivityPartnerDetail;
	}

	@Transactional
	public InclusivityTransactionData getTransactionByMfstransId(String transaction_id) {

		InclusivityTransactionData inclusivityTransactionData = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivityTransactionData = (InclusivityTransactionData) session
					.createQuery("From InclusivityTransactionData where mfs_trans_id=:mfs_trans_id")
					.setParameter("mfs_trans_id", transaction_id).uniqueResult();

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in getTransactionByMfstransId" + e);

		}
		return inclusivityTransactionData;
	}

	@Transactional
	public List<InclusivityTransactionData> getRetryTransactionData() {

		List<InclusivityTransactionData> inclusivityTransactionData = null;

		Session session = sessionFactory.getCurrentSession();

		try {

			inclusivityTransactionData = (List<InclusivityTransactionData>) session.createQuery(
					"From InclusivityTransactionData where responseCode LIKE '5%' or responseMessage='pending' ")
					.list();

			// .createQuery("From InclusivityTransactionData where response_code=" +
			// responseCode).list();

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in getRetryTransactionData list" + e);
		}
		return inclusivityTransactionData;
	}

	@Transactional
	public List<InclusivitySMSLogQueue> getSmsRecord() {
		List<InclusivitySMSLogQueue> inclusivitySMSLogQueue = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivitySMSLogQueue = (List<InclusivitySMSLogQueue>) session.createQuery("From InclusivitySMSLogQueue")
					.list();

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in getSmsRecordByMsidn" + e);
		}
		return inclusivitySMSLogQueue;
	}

	@Transactional
	public List<InclusivitySMSLogSuccess> getSmsLogSuccessData() {
		List<InclusivitySMSLogSuccess> inclusivitySMSLogSuccess = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivitySMSLogSuccess = (List<InclusivitySMSLogSuccess>) session
					.createQuery("From InclusivitySMSLogSuccess").list();

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in getSmsLogSuccessData" + e);
		}
		return inclusivitySMSLogSuccess;
	}

	@Transactional
	public InclusivitySMSLogQueue deleteLogQueueDataByMsisdn(String msisdn) {
		InclusivitySMSLogQueue inclusivitySMSLogQueue = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivitySMSLogQueue = (InclusivitySMSLogQueue) session
					.createQuery("From InclusivitySMSLogQueue where msisdn=:msisdn").setParameter("msisdn", msisdn)
					.uniqueResult();

			if (inclusivitySMSLogQueue != null) {
				session.delete(inclusivitySMSLogQueue);
			}
		

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in deleteLogQueueDataByMsisdn" + e);

		}
		return inclusivitySMSLogQueue;
	}

	@Transactional
	public InclusivityPushJobTrack getSchedulerData() {
		InclusivityPushJobTrack inclusivityPushJobTrack = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivityPushJobTrack = (InclusivityPushJobTrack) session
					.createQuery(
							"From InclusivityPushJobTrack where cronStatus='Success' order by push_job_track_Id  DESC ")
					.setMaxResults(1).uniqueResult();

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in deleteLogQueueDataByMsisdn" + e);

		}
		return inclusivityPushJobTrack;
	}

	@Transactional
	public void deleteSmsInQueueById(int smsId) {

		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from InclusivitySMSLogQueue where queueId= :smsId ";
		Query query = session.createQuery(hql);
		query.setInteger("queueId", smsId);
		query.executeUpdate();

	}

	@Transactional
	public InclusivityAuthorizationModel getTokenData() {
		InclusivityAuthorizationModel inclusivityAuthorizationModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			inclusivityAuthorizationModel = (InclusivityAuthorizationModel) session
					.createQuery(
							"From InclusivityAuthorizationModel order by auth_id  DESC ")
					.list().get(0);

		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in TransactionDaoImpl in deleteLogQueueDataByMsisdn" + e);

		}
		return inclusivityAuthorizationModel;
	}

}