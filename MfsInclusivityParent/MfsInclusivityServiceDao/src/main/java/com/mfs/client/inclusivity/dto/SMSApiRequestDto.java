package com.mfs.client.inclusivity.dto;

public class SMSApiRequestDto {

	private String msisdn;
	private String message;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SMSApiRequestDto [msisdn=" + msisdn + ", message=" + message + "]";
	}

}
