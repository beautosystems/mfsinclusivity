package com.mfs.client.inclusivity.service.impl;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.CurbiRequestDto;
import com.mfs.client.inclusivity.dto.TransactionDataDto;
import com.mfs.client.inclusivity.models.InclusivityPartnerCountry;
import com.mfs.client.inclusivity.models.InclusivityPushJobTrack;
import com.mfs.client.inclusivity.models.InclusivityTransPushLog;
import com.mfs.client.inclusivity.models.InclusivityTransactionData;
import com.mfs.client.inclusivity.service.TransactionDataService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.CommonValidations;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;

@Service("TransactionDataService")
public class TransactionDataServiceImpl implements TransactionDataService {

	private static final Logger LOGGER = Logger.getLogger(TransactionDataServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public List<TransactionDataDto> getCurbiTransaction() {

		String startDate = null;
		Timestamp endDate = new Timestamp(System.currentTimeMillis());
		Map<String, String> configMap = null;
		String partnerCode = null;
		String curbiResponse = null;
		InclusivityPartnerCountry inclusivityPartnerCountry = null;
		List<TransactionDataDto> transactionDataDto = null;

		try {

			InclusivityPushJobTrack inclusivityPushJobTrack = transactionDao.getSchedulerData();

			if (null != inclusivityPushJobTrack) {

				if (null != inclusivityPushJobTrack.getLastSuccessDateTime()) {
					startDate = inclusivityPushJobTrack.getLastSuccessDateTime() + "";
				}
			}
			configMap = inclusivitySystemConfigDao.getConfigDetailsMap();

			inclusivityPartnerCountry = transactionDao.getPartnerDetail();
			if (null != inclusivityPartnerCountry) {

				if (CommonValidations.isStringNotEmptyAndNotNull(inclusivityPartnerCountry.getPartnerCode())) {
					partnerCode = inclusivityPartnerCountry.getPartnerCode();
				}

			}

			CurbiRequestDto curbiRequestDto = new CurbiRequestDto();
			curbiRequestDto.setPartnerCode(partnerCode);
			if (null != startDate) {
				curbiRequestDto.setStartDate(startDate);
			} else {

				curbiRequestDto.setStartDate(startDate);
			}
			curbiRequestDto.setEndDate(endDate + "");
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			Gson gson = new Gson();
			connectionRequest.setHttpmethodName(CommonConstant.POST);
			if (CommonValidations.isStringNotEmptyAndNotNull(configMap.get(CommonConstant.PORT))) {
				connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			}
			connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL) + CommonConstant.CURBI_ENDPOINT);

			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

			connectionRequest.setHeaders(headerMap);

			LOGGER.info("==>before call service " + connectionRequest +curbiRequestDto);
			
			curbiResponse = CallServices.getResponseFromService(connectionRequest, gson.toJson(curbiRequestDto));
			
			LOGGER.info("==>after call service " + curbiResponse);

			if (CommonValidations.isStringNotEmptyAndNotNull(curbiResponse)
					&& !(curbiResponse.equalsIgnoreCase("[]"))) {
				Type collectionType = new TypeToken<List<TransactionDataDto>>() {
				}.getType();
				transactionDataDto = gson.fromJson(curbiResponse, collectionType);
				if (null != transactionDataDto && !(transactionDataDto.isEmpty())) {

					logCurbiCount(transactionDataDto.size(), inclusivityPartnerCountry.getPartnerCode());

					logSchedulerData(endDate, responseCodes.getProperty(CommonConstant.SUCCESS_CODE));
					for (TransactionDataDto dataDto : transactionDataDto) {
						logInclusivityResponseData(dataDto);
					}

				} else {
					LOGGER.error("==>not parseing proper " + transactionDataDto);
				}

			}

			else {
				logSchedulerData(endDate, responseCodes.getProperty(CommonConstant.CURBI_DATA));
				LOGGER.error("==>null response from curbi " + curbiResponse);
			}

		} catch (Exception e) {

			LOGGER.error("==>Exception in pushRemitTransaction" + e);

		}
		return transactionDataDto;
	}

	private void logSchedulerData(Timestamp startDate, String status) {
		InclusivityPushJobTrack InclusivityPushJobTrack1 = new InclusivityPushJobTrack();
		InclusivityPushJobTrack1.setCronStatus(status);
		InclusivityPushJobTrack1.setLastSuccessDateTime(startDate);
		transactionDao.save(InclusivityPushJobTrack1);
	}

	private void logCurbiCount(int count, String partnerCode) {
		InclusivityTransPushLog inclusivityTransPushLog = new InclusivityTransPushLog();
		inclusivityTransPushLog.setDateTimePushed(new Date());
		inclusivityTransPushLog.setPartnerCode(partnerCode);
		inclusivityTransPushLog.setTotRecordPushed(count);
		transactionDao.save(inclusivityTransPushLog);
	}

	private void logInclusivityResponseData(TransactionDataDto transactionDataDto1) {

		InclusivityTransactionData inclusivityTransactionData1 = new InclusivityTransactionData();
		inclusivityTransactionData1.setAmount(transactionDataDto1.getAmount_in_cents());
		inclusivityTransactionData1.setCurrency(transactionDataDto1.getCurrency());
		inclusivityTransactionData1.setMfsTransId(transactionDataDto1.getTransaction_id());
		inclusivityTransactionData1.setDateOfBirth(transactionDataDto1.getDate_of_birth());
		inclusivityTransactionData1.setFullName(transactionDataDto1.getFull_name());
		inclusivityTransactionData1.setMsisdn(transactionDataDto1.getMsisdn());
		inclusivityTransactionData1.setDateLogged(new Date());
		inclusivityTransactionData1.setCount(1);
		inclusivityTransactionData1.setResponseMessage(responseCodes.getProperty(CommonConstant.PENDING));
		transactionDao.save(inclusivityTransactionData1);
	}

}
