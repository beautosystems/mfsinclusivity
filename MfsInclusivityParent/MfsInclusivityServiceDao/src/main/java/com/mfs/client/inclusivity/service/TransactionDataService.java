package com.mfs.client.inclusivity.service;

import java.util.List;

import com.mfs.client.inclusivity.dto.TransactionDataDto;

public interface TransactionDataService {

	public List<TransactionDataDto> getCurbiTransaction();

}
