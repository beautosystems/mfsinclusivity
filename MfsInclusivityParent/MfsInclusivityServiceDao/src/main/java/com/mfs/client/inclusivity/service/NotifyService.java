package com.mfs.client.inclusivity.service;

import java.util.List;

import com.mfs.client.inclusivity.dto.SmsResponse;

public interface NotifyService {

	public List<SmsResponse> sendSMS();
}