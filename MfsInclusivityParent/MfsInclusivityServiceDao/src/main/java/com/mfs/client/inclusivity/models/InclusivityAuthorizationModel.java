package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_authorization")
public class InclusivityAuthorizationModel {

	@Id
	@GeneratedValue
	@Column(name = "auth_id")
	private int authId;

	@Column(name = "access_token", length = 250)
	private String accessToken;

	@Column(name = "token_type", length = 20)
	private String tokenType;

	@Column(name = "expires_in", length = 20)
	private String expiresIn;

	@Column(name = "scope", length = 20)
	private String scope;

	@Column(name = "user_client_name", length = 20)
	private String userClientName;

	@Column(name = "user_principle", length = 20)
	private String userPrinciple;
	
	@Column(name = "date_logged", length = 20)
	private Date dateLogged;

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getUserClientName() {
		return userClientName;
	}

	public void setUserClientName(String userClientName) {
		this.userClientName = userClientName;
	}

	public String getUserPrinciple() {
		return userPrinciple;
	}

	public void setUserPrinciple(String userPrinciple) {
		this.userPrinciple = userPrinciple;
	}

	
	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "InclusivityAuthorizationModel [authId=" + authId + ", accessToken=" + accessToken + ", tokenType="
				+ tokenType + ", expiresIn=" + expiresIn + ", scope=" + scope + ", userClientName=" + userClientName
				+ ", userPrinciple=" + userPrinciple + ", dateLogged=" + dateLogged + "]";
	}

}
