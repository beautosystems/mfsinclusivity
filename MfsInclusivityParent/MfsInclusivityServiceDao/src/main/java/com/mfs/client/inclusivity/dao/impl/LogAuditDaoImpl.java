package com.mfs.client.inclusivity.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.inclusivity.dao.LogAuditDao;
import com.mfs.client.inclusivity.models.InclusivityTransactionAudit;

@Repository("LogAuditDao")
public class LogAuditDaoImpl implements LogAuditDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger LOGGER = Logger.getLogger(LogAuditDaoImpl.class);

	@Transactional
	public long logAuditDetails(InclusivityTransactionAudit audit) {

		long auditId = 0;

		LOGGER.debug("Inside logAuditDetails of LogAuditDaoImpl");
		Session session = sessionFactory.getCurrentSession();
		auditId = (Long) session.save(audit);
		LOGGER.info("auditSave Save "+auditId);
		return auditId;
	
	}

}
