package com.mfs.client.inclusivity.dto;

public class PremiumsResponseDto {

	private String guid;
	private String granularity;
	private int cardinality;
	private String product_guid;
	private int amount_in_cents;
	private String payment_method;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getGranularity() {
		return granularity;
	}

	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}

	public int getCardinality() {
		return cardinality;
	}

	public void setCardinality(int cardinality) {
		this.cardinality = cardinality;
	}

	public String getProduct_guid() {
		return product_guid;
	}

	public void setProduct_guid(String product_guid) {
		this.product_guid = product_guid;
	}

	public int getAmount_in_cents() {
		return amount_in_cents;
	}

	public void setAmount_in_cents(int amount_in_cents) {
		this.amount_in_cents = amount_in_cents;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	@Override
	public String toString() {
		return "PremiumsResponseDto [guid=" + guid + ", granularity=" + granularity + ", cardinality=" + cardinality
				+ ", product_guid=" + product_guid + ", amount_in_cents=" + amount_in_cents + ", payment_method="
				+ payment_method + "]";
	}

}
