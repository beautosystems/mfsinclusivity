package com.mfs.client.inclusivity.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.dto.StatusResponseDto;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.models.InclusivityStatusModel;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.service.CustomerStatusService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.ExpiredToken;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;
import com.mfs.client.inclusivity.util.InclusivityCodes;

@Service("CustomerStatusService")
public class CustomerStatusServiceImpl implements CustomerStatusService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(CustomerStatusServiceImpl.class);

	public StatusResponseDto getStatus(String msisdn) {

		Gson gson = new Gson();
		StatusResponseDto statusResponse = new StatusResponseDto();
		Map<String, String> headerMap = new HashMap<String, String>();
		String inclusivityResponse = null;
		Map<String, String> configMap = null;
		String token = null;
		AuthorizationResponseDto response = null;

		try {

			// getting access token data from db
			InclusivityAuthorizationModel inclusivityAuthorizationModel = transactionDao.getTokenData();

			// checking null value and expiration for token
			if (null != inclusivityAuthorizationModel && ExpiredToken.isTokenExpire(inclusivityAuthorizationModel)) {

				// getting access token from db
				token = inclusivityAuthorizationModel.getAccessToken();
			} else {
				response = new AuthorizationResponseDto();
				// calling auth service
				response = authorizationService.authorizationService();
				// checking null response
				if (null != response) {

					// logging auth response in db
					authorizationService.logResponse(response);
					token = response.getAccess_token();
				}

				else {
					LOGGER.error("==>Null auth response " + response);

				}

			}

			// getting data from config table
			configMap = inclusivitySystemConfigDao.getConfigDetailsMap();

			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			String partnerGuId = configMap.get(CommonConstant.PARTNER_GUID);
			connectionRequest.setHttpmethodName(CommonConstant.GET);
			connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));

			connectionRequest.setServiceUrl(configMap.get(CommonConstant.INCLUSIVITY_URL)
					+ configMap.get(CommonConstant.CUSTOMERS) + "/" + msisdn + configMap.get(CommonConstant.STATUS)
					+ CommonConstant.PARTNERS + partnerGuId + CommonConstant.LOYALTY_ELIGIBILITY_INCLUDED + false
					+ CommonConstant.ACCESS_TOKENS + token);
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			connectionRequest.setHeaders(headerMap);

			// calling inclusitivity
			inclusivityResponse = CallServices.getResponseFromService(connectionRequest, null);

			LOGGER.info("==> customer status response " + inclusivityResponse);

			// checking null response
			if (null == inclusivityResponse) {

				LOGGER.error("==>Null customer status response " + inclusivityResponse);

				statusResponse.setErrorCode(InclusivityCodes.ER201.getCode());
				statusResponse.setErrorMessage(InclusivityCodes.ER201.getMessage());

			}

			// parsing object into json
			statusResponse = gson.fromJson(inclusivityResponse, StatusResponseDto.class);

			logResponse(statusResponse);

		} catch (Exception e) {

			LOGGER.error("==>Exception in CustomerStatusServiceImpl" + e);

			statusResponse.setErrorCode(Integer.parseInt(CommonConstant.EXCEPTION));
			statusResponse.setErrorMessage(responseCodes.getProperty(CommonConstant.EXCEPTION));

		}

		return statusResponse;
	}

	// log customer status response in db
	private void logResponse(StatusResponseDto statusResponse) {

		// check for registered and unregistered customer status
		if (statusResponse.getRegistered().equalsIgnoreCase("true")) {

			InclusivityStatusModel inclusivityStatus = new InclusivityStatusModel();

			inclusivityStatus.setServiceType(CommonConstant.CUSTOMER_STATUS);
			inclusivityStatus.setUssd_whitelist_active(statusResponse.getUssd_whitelist_active());
			inclusivityStatus.setGuid(statusResponse.getGuid());
			inclusivityStatus.setMsisdn(statusResponse.getMsisdn());
			inclusivityStatus.setNational_id(statusResponse.getNational_id());
			inclusivityStatus.setFull_name(statusResponse.getFull_name());
			inclusivityStatus.setDisplay_language(statusResponse.getDisplay_language());
			inclusivityStatus.setLanguage(statusResponse.getLanguage());
			inclusivityStatus.setDate_of_birth(statusResponse.getDate_of_birth());
			inclusivityStatus.setAgent(statusResponse.getAgent());
			inclusivityStatus.setAccept_agent_loyalty(statusResponse.getAccept_agent_loyalty());
			inclusivityStatus.setPayment_method(statusResponse.getPayment_method());
			inclusivityStatus.setRegistered(statusResponse.getRegistered());
			inclusivityStatus.setExperiment(statusResponse.getExperiment());
			inclusivityStatus.setDisplay_date_format_pattern(statusResponse.getDisplay_date_format_pattern());
			inclusivityStatus.setCall_centre_number(statusResponse.getCall_centre_number());
			inclusivityStatus.setService_name(statusResponse.getService_name());
			inclusivityStatus.setCheck_age_range(statusResponse.getCheck_age_range());
			inclusivityStatus.setUssd_shortcode(statusResponse.getUssd_shortcode());
			inclusivityStatus.setWhats_app_number(statusResponse.getWhats_app_number());
			inclusivityStatus.setMinimum_age(statusResponse.getMinimum_age());
			inclusivityStatus.setMaximum_age(statusResponse.getMaximum_age());
			inclusivityStatus.setGeneral_waiting_period(statusResponse.getGeneral_waiting_period());
			inclusivityStatus.setCurrent_month(statusResponse.getCurrent_month());
			inclusivityStatus.setPrevious_month(statusResponse.getPrevious_month());
			inclusivityStatus.setMessage(statusResponse.getMessage());
			inclusivityStatus.setDateLogged(new Date());

			transactionDao.save(inclusivityStatus);

		} else {

			InclusivityStatusModel inclusivityStatus = new InclusivityStatusModel();

			inclusivityStatus.setServiceType(CommonConstant.CUSTOMER_STATUS);
			inclusivityStatus.setUssd_whitelist_active(statusResponse.getUssd_whitelist_active());
			inclusivityStatus.setMsisdn(statusResponse.getMsisdn());
			inclusivityStatus.setRegistered(statusResponse.getRegistered());
			inclusivityStatus.setExperiment(statusResponse.getExperiment());
			inclusivityStatus.setDisplay_language(statusResponse.getDisplay_language());
			inclusivityStatus.setDisplay_date_format_pattern(statusResponse.getDisplay_date_format_pattern());
			inclusivityStatus.setCall_centre_number(statusResponse.getCall_centre_number());
			inclusivityStatus.setService_name(statusResponse.getService_name());
			inclusivityStatus.setCheck_age_range(statusResponse.getCheck_age_range());
			inclusivityStatus.setUssd_shortcode(statusResponse.getUssd_shortcode());
			inclusivityStatus.setWhats_app_number(statusResponse.getWhats_app_number());
			inclusivityStatus.setMinimum_age(statusResponse.getMinimum_age());
			inclusivityStatus.setMaximum_age(statusResponse.getMaximum_age());
			inclusivityStatus.setGeneral_waiting_period(statusResponse.getGeneral_waiting_period());
			inclusivityStatus.setCurrent_month(statusResponse.getCurrent_month());
			inclusivityStatus.setPrevious_month(statusResponse.getPrevious_month());
			inclusivityStatus.setMessage(statusResponse.getMessage());
			inclusivityStatus.setDateLogged(new Date());

			transactionDao.save(inclusivityStatus);
		}

	}

}
