package com.mfs.client.inclusivity.util;

public enum InclusivityCodes {

	S200(200 , "OK"),
	S202(202 , "Accepted"),
	ER201(201 ,"MFS System Error "),
	ER400(400 ,"Bad Request");
	

	private int code;
	private String message;

	private InclusivityCodes(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
}
