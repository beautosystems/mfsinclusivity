package com.mfs.client.inclusivity.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.service.ClaimService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.CommonValidations;
import com.mfs.client.inclusivity.util.ExpiredToken;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;
import com.mfs.client.inclusivity.util.InclusivityCodes;

@Service("ClaimService")
public class ClaimServiceImpl implements ClaimService {

	private static final Logger LOGGER = Logger.getLogger(ClaimServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;
	
	@Resource(name = "responseC")
	private Properties responseCodes;

	public ResponseStatus claim(String msisdn) {

		ResponseStatus responseStatus = new ResponseStatus();
		Map<String, String> headerMap = new HashMap<String, String>();
		Map<String, String> configMap = null;
		String inclusivityResponse = null;
		String token = null;
		AuthorizationResponseDto authResponse = null;

		try {

			// getting access token data from db
			InclusivityAuthorizationModel inclusivityAuthorizationModel = transactionDao.getTokenData();

			// checking null value and expiration for token
			if (null != inclusivityAuthorizationModel && ExpiredToken.isTokenExpire(inclusivityAuthorizationModel)) {

				// getting access token from db
				token = inclusivityAuthorizationModel.getAccessToken();
			} else {
				authResponse = new AuthorizationResponseDto();

				// calling auth service
				authResponse = authorizationService.authorizationService();
				// checking null response
				if (null != authResponse) {

					// logging auth response in db
					authorizationService.logResponse(authResponse);
					token = authResponse.getAccess_token();
				}

				else {
					LOGGER.error("==>Null auth response " + authResponse);

				}

			}

			// getting data from config table
			configMap = inclusivitySystemConfigDao.getConfigDetailsMap();
			if (configMap == null) {

				LOGGER.error("No Config Details in DB" + configMap);
			}

			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			String partnerGuId = configMap.get(CommonConstant.PARTNER_GUID);
			connectionRequest.setHttpmethodName(CommonConstant.POST);
			connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			connectionRequest.setServiceUrl(configMap.get(CommonConstant.INCLUSIVITY_URL)
					+ configMap.get(CommonConstant.REMITANCE_ENDPOINT) + configMap.get(CommonConstant.CLAIM_ENDPOINT)
					+ "/" + msisdn + CommonConstant.ACCESS_TOKEN + token + CommonConstant.PARTNER + partnerGuId);
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			connectionRequest.setHeaders(headerMap);

			// calling inclusitivity
			inclusivityResponse = CallServices.getResponseFromService(connectionRequest, null);

			LOGGER.info("==>Claim Service response " + inclusivityResponse);

			// checking null response for inclusivity
			if (CommonValidations.isStringNotEmptyAndNotNull(inclusivityResponse)) {

				if (inclusivityResponse.equals(CommonConstant.ICL_SUCCESS_CODE)) {

					responseStatus.setStatusCode(inclusivityResponse);
					responseStatus.setStatusMessage(InclusivityCodes.S202.getMessage());

				} else if (inclusivityResponse.equals(CommonConstant.ICL_FAIL_CODE)
						|| (inclusivityResponse.charAt(0)) == '5') {

					if (inclusivityResponse.charAt(0) == CommonConstant.FAIL_ERROR_CHECK) {

						responseStatus.setStatusCode(inclusivityResponse);
						responseStatus.setStatusMessage(CommonConstant.RESPONSEMESSAGE2);
					} else {

						responseStatus.setStatusCode(CommonConstant.ICL_FAIL_CODE);
						responseStatus.setStatusMessage(CommonConstant.RESPONSEMESSAGE4);
					}

				} else if(inclusivityResponse.equals(CommonConstant.ER404)) {

					responseStatus.setStatusCode(CommonConstant.ER404);
					responseStatus.setStatusMessage(CommonConstant.RESPONSEMESSAGE5);
				}else {
					
					responseStatus.setStatusCode(inclusivityResponse);
					responseStatus.setStatusMessage(CommonConstant.INCLUSIVITY_ERROR);
				}
					

			} else {

				LOGGER.error("==>null response from inclusivity " + inclusivityResponse);
			}

		} catch (Exception e) {

			LOGGER.error("==>Exception in ClaimServiceImpl" + e);
			responseStatus.setStatusCode(CommonConstant.EXCEPTION);
			responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.EXCEPTION));
		}

		return responseStatus;
	}

}