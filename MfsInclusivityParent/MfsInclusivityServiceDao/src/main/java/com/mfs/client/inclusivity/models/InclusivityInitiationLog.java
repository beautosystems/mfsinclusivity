package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_initiation_log")
public class InclusivityInitiationLog {

@Id
@GeneratedValue
@Column(name = "initiation_log_id")
private int initiationLogId;

@Column(name = "msisdn", length = 20)
private String msisdn;

@Column(name = "date_time_initiated")
private Date dateTimeInitiated;

@Column(name = "partner_code", length = 50)
private String partnerCode;

@Column(name = "service_name", length = 20)
private String serviceName;

@Column(name = "incl_resp_code", length = 20)
private String inclRespCode;

@Column(name = "incl_resp_message", length = 250)
private String inclRespMessage;

@Column(name = "status", length = 20)
private String status;

public int getInitiationLogId() {
return initiationLogId;
}

public void setInitiationLogId(int initiationLogId) {
this.initiationLogId = initiationLogId;
}

public String getMsisdn() {
return msisdn;
}

public void setMsisdn(String msisdn) {
this.msisdn = msisdn;
}

public Date getDateTimeInitiated() {
return dateTimeInitiated;
}

public void setDateTimeInitiated(Date dateTimeInitiated) {
this.dateTimeInitiated = dateTimeInitiated;
}

public String getPartnerCode() {
return partnerCode;
}

public void setPartnerCode(String partnerCode) {
this.partnerCode = partnerCode;
}

public String getServiceName() {
return serviceName;
}

public void setServiceName(String serviceName) {
this.serviceName = serviceName;
}

public String getInclRespCode() {
return inclRespCode;
}

public void setInclRespCode(String inclRespCode) {
this.inclRespCode = inclRespCode;
}

public String getInclRespMessage() {
return inclRespMessage;
}

public void setInclRespMessage(String inclRespMessage) {
this.inclRespMessage = inclRespMessage;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

@Override
public String toString() {
return "InclusivityInitiationLog [initiationLogId=" + initiationLogId + ", msisdn=" + msisdn
+ ", dateTimeInitiated=" + dateTimeInitiated + ", partnerCode=" + partnerCode + ", serviceName="
+ serviceName + ", inclRespCode=" + inclRespCode + ", inclRespMessage=" + inclRespMessage + ", status="
+ status + "]";
}
}