package com.mfs.client.inclusivity.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;


public class HttpConnectorImpl {

	public HttpsConnectionResponse httpUrlConnection(HttpsConnectionRequest httpRequest, String requestText)
			throws Exception {
		HttpURLConnection httpCon = null;
		URL url = null;
		StringBuffer response = null;

		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		try {
			url = new URL(httpRequest.getServiceUrl());

			httpCon = (HttpURLConnection) url.openConnection();

			httpCon.setRequestMethod(httpRequest.getHttpmethodName());
			httpCon.setRequestProperty("Content-Type", "application/json");

			httpCon.setDoOutput(true);

			httpCon.setInstanceFollowRedirects(false);
			httpCon.setUseCaches(false);

			setHeaderPropertiesToHTTPConnection(httpRequest, requestText, httpCon);

			if (requestText != null) {
				DataOutputStream wr = null;
				try {
					wr = new DataOutputStream(httpCon.getOutputStream());
					wr.writeBytes(requestText);
				} catch (Exception e) {

					throw new Exception(e);
				} finally {
					if (wr != null) {
						wr.flush();
						wr.close();
					}
				}
			}

			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));

			String inputLine = null;

			response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			connectionResponse.setResponseData(response.toString());

		} catch (Exception e) {

			throw new Exception(e);
		}

		return connectionResponse;
	}
	
	private void setHeaderPropertiesToHTTPConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpURLConnection httpURLConnection) {

		setHeadersToHTTPConnection(httpsConnectionRequest.getHeaders(), httpURLConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpURLConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpURLConnection.setRequestProperty("Content-Length", "0");
		}

	}
	
	private void setHeadersToHTTPConnection(Map<String, String> headers, HttpURLConnection httpURLConnection) {

		if (headers != null && !headers.isEmpty() && httpURLConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpURLConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}
}
