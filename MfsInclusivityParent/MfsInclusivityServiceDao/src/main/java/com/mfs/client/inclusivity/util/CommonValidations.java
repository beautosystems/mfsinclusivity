package com.mfs.client.inclusivity.util;

public class CommonValidations {
	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;
		String request = reqParamValue.trim();
		if (request == null || request.equals(""))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == 0 || amount == null)
			validateResult = true;

		return validateResult;
	}
	
	public static Boolean isStringNotEmptyAndNotNull(String arg) {
		if (arg != null && !("").equals(arg) && !("").equals(arg.trim())) {
			return true;
		} else {
			return false;
		}
	}
}
