package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.ResponseStatus;

public interface ClaimService {

	public  ResponseStatus claim(String msisdn);
	
}