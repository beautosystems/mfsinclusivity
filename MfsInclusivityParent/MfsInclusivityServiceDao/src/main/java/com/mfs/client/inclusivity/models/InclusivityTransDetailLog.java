package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_trans_detail_log")
public class InclusivityTransDetailLog {

	@Id
	@GeneratedValue
	@Column(name = "trans_detail_log_id")
	private int transDetailLogId;

	@Column(name = "partner_code", length = 50)
	private String partnerCode;

	@Column(name = "amount", length = 50)
	private String amount;

	@Column(name = "send_cur_code", length = 20)
	private String sendCurCode;

	@Column(name = "rec_cur_code", length = 20)
	private String recCurCode;

	@Column(name = "mfs_trans_id", length = 100)
	private String mfsTransId;

	@Column(name = "status", length = 50)
	private String status;

	@Column(name = "receiver_msisdn", length = 20)
	private String receiverMsisdn;

	@Column(name = "rec_first_name", length = 100)
	private String recFirstName;

	@Column(name = "rec_last_name", length = 100)
	private String recLastName;

	@Column(name = "status_message", length = 50)
	private String statusMessage;

	@Column(name = "inclusivity_trans_id", length = 100)
	private String inclusivityTransId;

	@Column(name = "rp_trans_id", length = 100)
	private String rpTransId;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getTransDetailLogId() {
		return transDetailLogId;
	}

	public void setTransDetailLogId(int transDetailLogId) {
		this.transDetailLogId = transDetailLogId;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSendCurCode() {
		return sendCurCode;
	}

	public void setSendCurCode(String sendCurCode) {
		this.sendCurCode = sendCurCode;
	}

	public String getRecCurCode() {
		return recCurCode;
	}

	public void setRecCurCode(String recCurCode) {
		this.recCurCode = recCurCode;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getRecFirstName() {
		return recFirstName;
	}

	public void setRecFirstName(String recFirstName) {
		this.recFirstName = recFirstName;
	}

	public String getRecLastName() {
		return recLastName;
	}

	public void setRecLastName(String recLastName) {
		this.recLastName = recLastName;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getInclusivityTransId() {
		return inclusivityTransId;
	}

	public void setInclusivityTransId(String inclusivityTransId) {
		this.inclusivityTransId = inclusivityTransId;
	}

	public String getRpTransId() {
		return rpTransId;
	}

	public void setRpTransId(String rpTransId) {
		this.rpTransId = rpTransId;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "InclusivityTransDetailLog [transDetailLogId=" + transDetailLogId + ", partnerCode=" + partnerCode
				+ ", amount=" + amount + ", sendCurCode=" + sendCurCode + ", recCurCode=" + recCurCode + ", mfsTransId="
				+ mfsTransId + ", status=" + status + ", receiverMsisdn=" + receiverMsisdn + ", recFirstName="
				+ recFirstName + ", recLastName=" + recLastName + ", statusMessage=" + statusMessage
				+ ", inclusivityTransId=" + inclusivityTransId + ", rpTransId=" + rpTransId + ", dateLogged="
				+ dateLogged + "]";
	}

}