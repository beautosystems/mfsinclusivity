package com.mfs.client.inclusivity.dto;

public class SendSms {
	private String to;
	private String from;
	private String text;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "SendSms [to=" + to + ", from=" + from + ", text=" + text + "]";
	}

}
