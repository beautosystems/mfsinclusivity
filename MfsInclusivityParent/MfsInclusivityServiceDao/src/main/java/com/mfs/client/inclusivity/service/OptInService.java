package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.ResponseStatus;

public interface OptInService {
	
	public ResponseStatus optIn(String msisdn);

}
