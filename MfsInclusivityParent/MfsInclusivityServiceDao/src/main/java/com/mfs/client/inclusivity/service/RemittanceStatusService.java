package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.StatusResponseDto;

public interface RemittanceStatusService {
	
	public StatusResponseDto remittanceStatus(String msisdn);

}
