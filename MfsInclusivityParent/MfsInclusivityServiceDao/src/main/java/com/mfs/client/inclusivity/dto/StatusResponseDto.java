package com.mfs.client.inclusivity.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class StatusResponseDto {

	private String ussd_whitelist_active;
	private String guid;
	private String msisdn;
	private String national_id;
	private String full_name;
	private String display_language;
	private String language;
	private String date_of_birth;
	private String agent;
	private String accept_agent_loyalty;
	private String payment_method;
	private String registered;
	private String experiment;
	private List<PremiumsResponseDto> premiums;
	private String display_date_format_pattern;
	private String call_centre_number;
	private String service_name;
	private String check_age_range;
	private List<LanguagesResponseDto> languages;
	private String ussd_shortcode;
	private String whats_app_number;
	private String minimum_age;
	private String maximum_age;
	private List<ProductsResponseDto> products;
	private String general_waiting_period;
	private String current_month;
	private String previous_month;
	private String message;
	private int errorCode;
	private String errorMessage;

	public String getUssd_whitelist_active() {
		return ussd_whitelist_active;
	}

	public void setUssd_whitelist_active(String ussd_whitelist_active) {
		this.ussd_whitelist_active = ussd_whitelist_active;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getRegistered() {
		return registered;
	}

	public void setRegistered(String registered) {
		this.registered = registered;
	}

	public String getExperiment() {
		return experiment;
	}

	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}

	public String getDisplay_language() {
		return display_language;
	}

	public void setDisplay_language(String display_language) {
		this.display_language = display_language;
	}

	public String getDisplay_date_format_pattern() {
		return display_date_format_pattern;
	}

	public void setDisplay_date_format_pattern(String display_date_format_pattern) {
		this.display_date_format_pattern = display_date_format_pattern;
	}

	public String getCall_centre_number() {
		return call_centre_number;
	}

	public void setCall_centre_number(String call_centre_number) {
		this.call_centre_number = call_centre_number;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public String getCheck_age_range() {
		return check_age_range;
	}

	public void setCheck_age_range(String check_age_range) {
		this.check_age_range = check_age_range;
	}

	public List<LanguagesResponseDto> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguagesResponseDto> languages) {
		this.languages = languages;
	}

	public String getUssd_shortcode() {
		return ussd_shortcode;
	}

	public void setUssd_shortcode(String ussd_shortcode) {
		this.ussd_shortcode = ussd_shortcode;
	}

	public String getWhats_app_number() {
		return whats_app_number;
	}

	public void setWhats_app_number(String whats_app_number) {
		this.whats_app_number = whats_app_number;
	}

	public String getMinimum_age() {
		return minimum_age;
	}

	public void setMinimum_age(String minimum_age) {
		this.minimum_age = minimum_age;
	}

	public String getMaximum_age() {
		return maximum_age;
	}

	public void setMaximum_age(String maximum_age) {
		this.maximum_age = maximum_age;
	}

	public List<ProductsResponseDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductsResponseDto> products) {
		this.products = products;
	}

	public String getGeneral_waiting_period() {
		return general_waiting_period;
	}

	public void setGeneral_waiting_period(String general_waiting_period) {
		this.general_waiting_period = general_waiting_period;
	}

	public String getCurrent_month() {
		return current_month;
	}

	public void setCurrent_month(String current_month) {
		this.current_month = current_month;
	}

	public String getPrevious_month() {
		return previous_month;
	}

	public void setPrevious_month(String previous_month) {
		this.previous_month = previous_month;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNational_id() {
		return national_id;
	}

	public void setNational_id(String national_id) {
		this.national_id = national_id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getAccept_agent_loyalty() {
		return accept_agent_loyalty;
	}

	public void setAccept_agent_loyalty(String accept_agent_loyalty) {
		this.accept_agent_loyalty = accept_agent_loyalty;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public List<PremiumsResponseDto> getPremiums() {
		return premiums;
	}

	public void setPremiums(List<PremiumsResponseDto> premiums) {
		this.premiums = premiums;
	}

	@Override
	public String toString() {
		return "StatusResponseDto [ussd_whitelist_active=" + ussd_whitelist_active + ", guid=" + guid + ", msisdn="
				+ msisdn + ", national_id=" + national_id + ", full_name=" + full_name + ", display_language="
				+ display_language + ", language=" + language + ", date_of_birth=" + date_of_birth + ", agent=" + agent
				+ ", accept_agent_loyalty=" + accept_agent_loyalty + ", payment_method=" + payment_method
				+ ", registered=" + registered + ", experiment=" + experiment + ", premiums=" + premiums
				+ ", display_date_format_pattern=" + display_date_format_pattern + ", call_centre_number="
				+ call_centre_number + ", service_name=" + service_name + ", check_age_range=" + check_age_range
				+ ", languages=" + languages + ", ussd_shortcode=" + ussd_shortcode + ", whats_app_number="
				+ whats_app_number + ", minimum_age=" + minimum_age + ", maximum_age=" + maximum_age + ", products="
				+ products + ", general_waiting_period=" + general_waiting_period + ", current_month=" + current_month
				+ ", previous_month=" + previous_month + ", message=" + message + ", errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + "]";
	}

}
