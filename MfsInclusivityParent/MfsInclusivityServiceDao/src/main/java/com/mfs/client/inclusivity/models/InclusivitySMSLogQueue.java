package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_sms_log_queue")
public class InclusivitySMSLogQueue {

	@Id
	@GeneratedValue
	@Column(name = "queue_id")
	private int queueId;

	@Column(name = "msisdn", length = 20)
	private String msisdn;

	@Column(name = "message", length = 250)
	private String message;

	@Column(name = "status", length = 20)
	private String status;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getQueueId() {
		return queueId;
	}

	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "InclusivitySMSLogQueue [queueId=" + queueId + ", msisdn=" + msisdn + ", message=" + message
				+ ", status=" + status + ", dateLogged=" + dateLogged + "]";
	}

}