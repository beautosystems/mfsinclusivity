package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "icl_status_loyalities")
public class InclusivityStatusLoyalities {

	@Id
	@GeneratedValue
	@Column(name = "status_loyalities_id")
	private int statusLoyalitiesId;

	@Column(name = "type", length = 20)
	private String type;

	@Column(name = "code", length = 20)
	private String code;

	@Column(name = "night_cover", length = 20)
	private String nightCover;

	@Column(name = "minimum_nights", length = 20)
	private String minimumNights;

	@Column(name = "maximum_nights", length = 20)
	private String maximumNights;

	@Column(name = "fixed_benefit_amount", length = 20)
	private String fixedBenefitAmount;

	@Column(name = "eligibility_threshold", length = 20)
	private String eligibilityThreshold;

	@Column(name = "waiting_period_days", length = 20)
	private String waitingPeriodDays;

	@Column(name = "minimum_age", length = 20)
	private String minimumAge;

	@Column(name = "maximum_age", length = 20)
	private String maximumAge;

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Column(name = "date_logged")
	private Date dateLogged;

	@OneToOne
	@JoinColumn(name = "status_detail_id")
	private InclusivityStatusDetail inclusivityStatusDetail;

	public int getStatusLoyalitiesId() {
		return statusLoyalitiesId;
	}

	public void setStatusLoyalitiesId(int statusLoyalitiesId) {
		this.statusLoyalitiesId = statusLoyalitiesId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNightCover() {
		return nightCover;
	}

	public void setNightCover(String nightCover) {
		this.nightCover = nightCover;
	}

	public String getMinimumNights() {
		return minimumNights;
	}

	public void setMinimumNights(String minimumNights) {
		this.minimumNights = minimumNights;
	}

	public String getMaximumNights() {
		return maximumNights;
	}

	public void setMaximumNights(String maximumNights) {
		this.maximumNights = maximumNights;
	}

	public String getFixedBenefitAmount() {
		return fixedBenefitAmount;
	}

	public void setFixedBenefitAmount(String fixedBenefitAmount) {
		this.fixedBenefitAmount = fixedBenefitAmount;
	}

	public String getEligibilityThreshold() {
		return eligibilityThreshold;
	}

	public void setEligibilityThreshold(String eligibilityThreshold) {
		this.eligibilityThreshold = eligibilityThreshold;
	}

	public String getWaitingPeriodDays() {
		return waitingPeriodDays;
	}

	public void setWaitingPeriodDays(String waitingPeriodDays) {
		this.waitingPeriodDays = waitingPeriodDays;
	}

	public String getMinimumAge() {
		return minimumAge;
	}

	public void setMinimumAge(String minimumAge) {
		this.minimumAge = minimumAge;
	}

	public String getMaximumAge() {
		return maximumAge;
	}

	public void setMaximumAge(String maximumAge) {
		this.maximumAge = maximumAge;
	}

	public InclusivityStatusDetail getInclusivityStatusDetail() {
		return inclusivityStatusDetail;
	}

	public void setInclusivityStatusDetail(InclusivityStatusDetail inclusivityStatusDetail) {
		this.inclusivityStatusDetail = inclusivityStatusDetail;
	}

	@Override
	public String toString() {
		return "InclusivityStatusLoyalities [statusLoyalitiesId=" + statusLoyalitiesId + ", type=" + type + ", code="
				+ code + ", nightCover=" + nightCover + ", minimumNights=" + minimumNights + ", maximumNights="
				+ maximumNights + ", fixedBenefitAmount=" + fixedBenefitAmount + ", eligibilityThreshold="
				+ eligibilityThreshold + ", waitingPeriodDays=" + waitingPeriodDays + ", minimumAge=" + minimumAge
				+ ", maximumAge=" + maximumAge + ", dateLogged=" + dateLogged + ", inclusivityStatusDetail="
				+ inclusivityStatusDetail + "]";
	}

}