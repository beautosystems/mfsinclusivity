package com.mfs.client.inclusivity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.models.InclusivitySystemConfigModel;

@EnableTransactionManagement
@Repository("InclusivitySystemConfigDao")
public class InclusivitySystemConfigDaoImpl implements InclusivitySystemConfigDao {

	private static final Logger LOGGER = Logger.getLogger(InclusivitySystemConfigDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public Map<String, String> getConfigDetailsMap() {
		// TODO Auto-generated method stub

		LOGGER.debug("Inside getConfigDetailsMap of InclusivitySystemConfigDaoImpl");

		Map<String, String> systemConfigurationMap = null;
		Session session = sessionFactory.getCurrentSession();

		String hql = " From InclusivitySystemConfigModel";
		Query query = session.createQuery(hql);
		List<InclusivitySystemConfigModel> systemConfigModel = query.list();

		if (systemConfigModel != null && !systemConfigModel.isEmpty()) {
			systemConfigurationMap = new HashMap<String, String>();
			for (InclusivitySystemConfigModel systemConfiguration : systemConfigModel) {
				systemConfigurationMap.put(systemConfiguration.getConfigKey(),
						systemConfiguration.getConfigValue());
			}
		}
		if (systemConfigurationMap != null && !systemConfigurationMap.isEmpty()) {
			LOGGER.debug("getConfigDetailsMap response size -> " + systemConfigurationMap.size());
		}
		return systemConfigurationMap;

	}

	
}