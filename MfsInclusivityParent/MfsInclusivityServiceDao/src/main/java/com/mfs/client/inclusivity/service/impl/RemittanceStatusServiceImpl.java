package com.mfs.client.inclusivity.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.dto.StatusResponseDto;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.models.InclusivityStatusModel;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.service.RemittanceStatusService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.ExpiredToken;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;
import com.mfs.client.inclusivity.util.InclusivityCodes;

@Service("RemittanceStatusService")
public class RemittanceStatusServiceImpl implements RemittanceStatusService {

	private static final Logger LOGGER = Logger.getLogger(RemittanceStatusServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public StatusResponseDto remittanceStatus(String msisdn) {

		Gson gson = new Gson();
		StatusResponseDto remittanceStatusResponse = new StatusResponseDto();
		Map<String, String> headerMap = new HashMap<String, String>();
		String inclusivityResponse = null;
		Map<String, String> configMap = null;
		String token = null;
		AuthorizationResponseDto response = null;

		try {

			// getting access token data from db
			InclusivityAuthorizationModel inclusivityAuthorizationModel = transactionDao.getTokenData();

			// checking null value and expiration for token
			if (null != inclusivityAuthorizationModel && ExpiredToken.isTokenExpire(inclusivityAuthorizationModel)) {

				// getting access token from db
				token = inclusivityAuthorizationModel.getAccessToken();
			} else {
				response = new AuthorizationResponseDto();
				// calling auth service
				response = authorizationService.authorizationService();
				// checking null response
				if (null != response) {

					// logging auth response in db
					authorizationService.logResponse(response);
					token = response.getAccess_token();
				}

				else {
					LOGGER.error("==>Null auth response " + response);

				}

			}

			// getting data from config table
			configMap = inclusivitySystemConfigDao.getConfigDetailsMap();

			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			String partnerGuId = configMap.get(CommonConstant.PARTNER_GUID);
			connectionRequest.setHttpmethodName(CommonConstant.GET);
			connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			connectionRequest.setServiceUrl(configMap.get(CommonConstant.INCLUSIVITY_URL)
					+ configMap.get(CommonConstant.REMITANCE_ENDPOINT) + configMap.get(CommonConstant.STATUS) + "/"
					+ msisdn + CommonConstant.ACCESS_TOKEN + token + CommonConstant.PARTNER + partnerGuId
					+ CommonConstant.LOYALTY_ELIGIBILITY_INCLUDED + true);
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			connectionRequest.setHeaders(headerMap);

			// calling inclusitivity
			inclusivityResponse = CallServices.getResponseFromService(connectionRequest, null);

			LOGGER.info("==>Remittance status response " + inclusivityResponse);

			// checking null response
			if (null == inclusivityResponse) {

				LOGGER.error("==>Null remittance status response " + inclusivityResponse);

				remittanceStatusResponse.setErrorCode(InclusivityCodes.ER201.getCode());
				remittanceStatusResponse.setErrorMessage(InclusivityCodes.ER201.getMessage());

			}

			// parsing object into json
			remittanceStatusResponse = gson.fromJson(inclusivityResponse, StatusResponseDto.class);

			logResponse(remittanceStatusResponse);

		} catch (Exception e) {

			LOGGER.error("==>Exception in RemittanceStatusServiceImpl" + e);

			remittanceStatusResponse.setErrorCode(Integer.parseInt(CommonConstant.EXCEPTION));
			remittanceStatusResponse.setErrorMessage(responseCodes.getProperty(CommonConstant.EXCEPTION));
		}

		return remittanceStatusResponse;
	}

	// log remittance status response in db
	private void logResponse(StatusResponseDto remittanceStatusResponse) {

		// check for registered and unregistered remittance status
		if (remittanceStatusResponse.getRegistered().equalsIgnoreCase("true")) {

			InclusivityStatusModel inclusivityStatus = new InclusivityStatusModel();

			inclusivityStatus.setServiceType(CommonConstant.REMITTANCE_STATUS);
			inclusivityStatus.setUssd_whitelist_active(remittanceStatusResponse.getUssd_whitelist_active());
			inclusivityStatus.setGuid(remittanceStatusResponse.getGuid());
			inclusivityStatus.setMsisdn(remittanceStatusResponse.getMsisdn());
			inclusivityStatus.setNational_id(remittanceStatusResponse.getNational_id());
			inclusivityStatus.setFull_name(remittanceStatusResponse.getFull_name());
			inclusivityStatus.setDisplay_language(remittanceStatusResponse.getDisplay_language());
			inclusivityStatus.setLanguage(remittanceStatusResponse.getLanguage());
			inclusivityStatus.setDate_of_birth(remittanceStatusResponse.getDate_of_birth());
			inclusivityStatus.setAgent(remittanceStatusResponse.getAgent());
			inclusivityStatus.setAccept_agent_loyalty(remittanceStatusResponse.getAccept_agent_loyalty());
			inclusivityStatus.setPayment_method(remittanceStatusResponse.getPayment_method());
			inclusivityStatus.setRegistered(remittanceStatusResponse.getRegistered());
			inclusivityStatus.setExperiment(remittanceStatusResponse.getExperiment());
			inclusivityStatus.setDisplay_date_format_pattern(remittanceStatusResponse.getDisplay_date_format_pattern());
			inclusivityStatus.setCall_centre_number(remittanceStatusResponse.getCall_centre_number());
			inclusivityStatus.setService_name(remittanceStatusResponse.getService_name());
			inclusivityStatus.setCheck_age_range(remittanceStatusResponse.getCheck_age_range());
			inclusivityStatus.setUssd_shortcode(remittanceStatusResponse.getUssd_shortcode());
			inclusivityStatus.setWhats_app_number(remittanceStatusResponse.getWhats_app_number());
			inclusivityStatus.setMinimum_age(remittanceStatusResponse.getMinimum_age());
			inclusivityStatus.setMaximum_age(remittanceStatusResponse.getMaximum_age());
			inclusivityStatus.setGeneral_waiting_period(remittanceStatusResponse.getGeneral_waiting_period());
			inclusivityStatus.setCurrent_month(remittanceStatusResponse.getCurrent_month());
			inclusivityStatus.setPrevious_month(remittanceStatusResponse.getPrevious_month());
			inclusivityStatus.setMessage(remittanceStatusResponse.getMessage());
			inclusivityStatus.setDateLogged(new Date());

			transactionDao.save(inclusivityStatus);

		} else {

			InclusivityStatusModel inclusivityStatus = new InclusivityStatusModel();

			inclusivityStatus.setServiceType(CommonConstant.REMITTANCE_STATUS);
			inclusivityStatus.setUssd_whitelist_active(remittanceStatusResponse.getUssd_whitelist_active());
			inclusivityStatus.setMsisdn(remittanceStatusResponse.getMsisdn());
			inclusivityStatus.setRegistered(remittanceStatusResponse.getRegistered());
			inclusivityStatus.setExperiment(remittanceStatusResponse.getExperiment());
			inclusivityStatus.setDisplay_language(remittanceStatusResponse.getDisplay_language());
			inclusivityStatus.setDisplay_date_format_pattern(remittanceStatusResponse.getDisplay_date_format_pattern());
			inclusivityStatus.setCall_centre_number(remittanceStatusResponse.getCall_centre_number());
			inclusivityStatus.setService_name(remittanceStatusResponse.getService_name());
			inclusivityStatus.setCheck_age_range(remittanceStatusResponse.getCheck_age_range());
			inclusivityStatus.setUssd_shortcode(remittanceStatusResponse.getUssd_shortcode());
			inclusivityStatus.setWhats_app_number(remittanceStatusResponse.getWhats_app_number());
			inclusivityStatus.setMinimum_age(remittanceStatusResponse.getMinimum_age());
			inclusivityStatus.setMaximum_age(remittanceStatusResponse.getMaximum_age());
			inclusivityStatus.setGeneral_waiting_period(remittanceStatusResponse.getGeneral_waiting_period());
			inclusivityStatus.setCurrent_month(remittanceStatusResponse.getCurrent_month());
			inclusivityStatus.setPrevious_month(remittanceStatusResponse.getPrevious_month());
			inclusivityStatus.setMessage(remittanceStatusResponse.getMessage());
			inclusivityStatus.setDateLogged(new Date());

			transactionDao.save(inclusivityStatus);
		}
	}

}
