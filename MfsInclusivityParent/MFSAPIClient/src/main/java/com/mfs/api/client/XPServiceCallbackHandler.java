/**
 * XPServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.6  Built on : Jul 30, 2017 (09:08:31 BST)
 */
package com.mfs.api.client;


/**
 *  XPServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class XPServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public XPServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public XPServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for bank_remit_log method
     * override this method for handling normal response from bank_remit_log operation
     */
    public void receiveResultbank_remit_log(
        XPServiceStub.Bank_remit_logResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from bank_remit_log operation
     */
    public void receiveErrorbank_remit_log(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for get_partners method
     * override this method for handling normal response from get_partners operation
     */
    public void receiveResultget_partners(
    	XPServiceStub.Get_partnersResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from get_partners operation
     */
    public void receiveErrorget_partners(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for cancel_trans method
     * override this method for handling normal response from cancel_trans operation
     */
    public void receiveResultcancel_trans(
        XPServiceStub.Cancel_transResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from cancel_trans operation
     */
    public void receiveErrorcancel_trans(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for get_banks method
     * override this method for handling normal response from get_banks operation
     */
    public void receiveResultget_banks(
        XPServiceStub.Get_banksResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from get_banks operation
     */
    public void receiveErrorget_banks(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for get_trans_status method
     * override this method for handling normal response from get_trans_status operation
     */
    public void receiveResultget_trans_status(
        XPServiceStub.Get_trans_statusResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from get_trans_status operation
     */
    public void receiveErrorget_trans_status(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for validate_airtime_account method
     * override this method for handling normal response from validate_airtime_account operation
     */
    public void receiveResultvalidate_airtime_account(
        XPServiceStub.Validate_airtime_accountResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from validate_airtime_account operation
     */
    public void receiveErrorvalidate_airtime_account(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for airtime_trans_log method
     * override this method for handling normal response from airtime_trans_log operation
     */
    public void receiveResultairtime_trans_log(
        XPServiceStub.Airtime_trans_logResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from airtime_trans_log operation
     */
    public void receiveErrorairtime_trans_log(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for get_rate method
     * override this method for handling normal response from get_rate operation
     */
    public void receiveResultget_rate(XPServiceStub.Get_rateResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from get_rate operation
     */
    public void receiveErrorget_rate(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for bank_trans_log method
     * override this method for handling normal response from bank_trans_log operation
     */
    public void receiveResultbank_trans_log(
        XPServiceStub.Bank_trans_logResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from bank_trans_log operation
     */
    public void receiveErrorbank_trans_log(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for airtime_trans_com method
     * override this method for handling normal response from airtime_trans_com operation
     */
    public void receiveResultairtime_trans_com(
        XPServiceStub.Airtime_trans_comResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from airtime_trans_com operation
     */
    public void receiveErrorairtime_trans_com(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for mm_trans_log method
     * override this method for handling normal response from mm_trans_log operation
     */
    public void receiveResultmm_trans_log(
        XPServiceStub.Mm_trans_logResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from mm_trans_log operation
     */
    public void receiveErrormm_trans_log(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for trans_com method
     * override this method for handling normal response from trans_com operation
     */
    public void receiveResulttrans_com(
        XPServiceStub.Trans_comResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from trans_com operation
     */
    public void receiveErrortrans_com(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for account_request method
     * override this method for handling normal response from account_request operation
     */
    public void receiveResultaccount_request(
        XPServiceStub.Account_requestResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from account_request operation
     */
    public void receiveErroraccount_request(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for mm_remit_log method
     * override this method for handling normal response from mm_remit_log operation
     */
    public void receiveResultmm_remit_log(
        XPServiceStub.Mm_remit_logResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from mm_remit_log operation
     */
    public void receiveErrormm_remit_log(java.lang.Exception e) {
    }
}
