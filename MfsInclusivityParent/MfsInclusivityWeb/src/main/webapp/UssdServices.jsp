<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Inclusivity</title>
</head>
<body>

	<center>
		<h2>Inclusivity</h2>
	</center>
	<div class="container">
		<center>
			<div class="content float-left"
				style="width: 50%; border: 1px solid; padding: 2%; box-shadow: 5px 5px grey;">
				<form action="" autocomplete="off">
					<div class="form-group ">
						<label for="email" style="float: left">Enter Msisdn :</label> <input
							type="text" class="form-control float-left" id="msisdn"
							placeholder="Enter Msisdn" name="msisdn" autocomplete="off">
					</div>


					<div class="form-group ">
						<label for="email" style="float: left">Enter Service Code
							:</label> <input type="text" class="form-control float-left"
							id="serviceCode" placeholder="Enter Service Code"
							name="Service Code">
					</div>
					<input type="button" class="btn btn-success" id="servicCodeBtn"
						value="Submit" /> </br>

					<p>Note : Get service code from below mentioned services. EX:
						Opt_in Service = 1, Customer Status = 2</p>

					<div class="checkbox float-left">
						<label style="float: left"> 1. Opt_in Service</label>
					</div>
					</br>


					<div class="checkbox">
						<label style="float: left"> 2. Customer Status Service</label>
					</div>
					</br>
					<div class="checkbox">
						<label style="float: left"> 3. Claim Service</label>
					</div>
					</br>

					<div class="checkbox">
						<label style="float: left"> 4. Opt_out Service</label>
					</div>
					</br>






				</form>
			</div>

		</center>
	</div>
</body>

<script>
	$("#servicCodeBtn").click(function() {

		var serviceCode = document.getElementById("serviceCode").value;

		if (serviceCode == "" || serviceCode == null) {
			alert("please enter service code");
			return;
		}

		else {

			if (serviceCode == "1") {
				optin();
			} else if (serviceCode == "2") {
				customerStatus();
			} else if (serviceCode == "3") {

				claim();
			} else if (serviceCode == "4") {
				optOut();
			}

			else {
				alert("Please Enter correct service code");
			}

		}
	});

	function optin() {

		// window.location = "http://www.google.com";

		var msisdn = document.getElementById("msisdn").value;


		if (msisdn == "" || msisdn == null) {
			alert("Please Enter Msisdn");
			return;
		}

		$.ajax({
			url : "http://localhost:8080/MfsInclusivityWeb/optin/" + msisdn,
			type : "get", //send it through get method
			/*   data: { 
			    ajaxid: 4, 
			    UserID: UserID, 
			    EmailAddress: EmailAddress
			  }, */
			success : function(response) {
				//Do Something
				var finalresponse = JSON.stringify(response);
				//document.getElementById('response').innerHTML = finalresponse;

				var w = window.open('about:blank', 'Inclusivity');
				w.document.write(finalresponse);
				w.document.close();
				//alert(finalresponse);
			},
			error : function(xhr) {
				//Do Something to handle error
				alert("error");
			}
		});

	}

	function optOut() {

		// window.location = "http://www.google.com";

		var msisdn = document.getElementById("msisdn").value;

		if (msisdn == "" || msisdn == null) {
			alert("Please Enter Msisdn");
			return;
		}
		$.ajax({
			url : "http://localhost:8080/MfsInclusivityWeb/optout/" + msisdn,
			type : "get", //send it through get method
			/*   data: { 
			    ajaxid: 4, 
			    UserID: UserID, 
			    EmailAddress: EmailAddress
			  }, */
			success : function(response) {
				//Do Something
				var finalresponse = JSON.stringify(response);
				var w = window.open('about:blank', 'Inclusivity');
				w.document.write(finalresponse);
				w.document.close();
				//alert("suceess");
			},
			error : function(xhr) {
				//Do Something to handle error
				alert("error");
			}
		});

	}

	function customerStatus() {
		//alert("inside onclick");
		// window.location = "http://www.google.com";

		var msisdn = document.getElementById("msisdn").value;

		if (msisdn == "" || msisdn == null) {
			alert("Please Enter Msisdn");
			return;
		}
		$.ajax({
			url : "http://localhost:8080/MfsInclusivityWeb/customerstatus/"
					+ msisdn,
			type : "get", //send it through get method
			/*   data: { 
			    ajaxid: 4, 
			    UserID: UserID, 
			    EmailAddress: EmailAddress
			  }, */
			success : function(response) {
				//Do Something
				//alert(response);
				var finalresponse = JSON.stringify(response);
				var finalresponse1 = JSON.parse(finalresponse);
				var w = window.open('about:blank', 'Inclusivity');
				w.document.write("Status : " + finalresponse1['registered']);
				w.document.close();
			},
			error : function(xhr) {
				//Do Something to handle error
				alert("error");
			}
		});

	}

	function claim() {

		var msisdn = document.getElementById("msisdn").value;

		if (msisdn == "" || msisdn == null) {
			alert("Please Enter Msisdn");
			return;
		}

		$.ajax({
			url : "http://localhost:8080/MfsInclusivityWeb/claim/" + msisdn,
			type : "get", //send it through get method
			/*   data: { 
			    ajaxid: 4, 
			    UserID: UserID, 
			    EmailAddress: EmailAddress
			  }, */
			success : function(response) {
				//Do Something
				var finalresponse = JSON.stringify(response);
				var w = window.open('about:blank', 'Inclusivity');
				w.document.write(finalresponse);
				w.document.close();
				//alert("suceess");
			},
			error : function(xhr) {
				//Do Something to handle error
				alert("error");
			}
		});

	}
</script>


</html>