<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Inclusivity</title>
</head>
<body>

	<center>
		<h2>Inclusivity</h2>
	</center>
	<div class="container">
		<center>
			<div class="content float-left"
				style="width: 50%; border: 1px solid; padding: 2%; box-shadow: 5px 5px grey;">
				<form action="">

					<div class="checkbox float-left">
						<label style="float: left"><input type="checkbox"
							name="remember" id="curbiTrans"> 1. Curbi Transaction
							Data</label>
					</div>
					</br>


					<div class="checkbox">
						<label style="float: left"><input type="checkbox"
							name="remember" id="pushRetry">2. Push And Retry
							Transaction</label>
					</div>
					</br>
					<div class="checkbox">
						<label style="float: left"><input type="checkbox"
							name="remember" id="sms"> 3. SMS Trigger</label>
					</div>
					</br>


					<div class="checkbox">
						<label style="float: left"><input type="checkbox"
							name="remember" id="sendSms"> 4.Send SMS </label>
					</div>
					</br>

					<div style="display: none;" id="hiddendiv">
						<form class="form-inline">
							<div class="form-group">
								<label for="email" style="float: left">Enter Msisdn :</label> <input
									type="text" class="form-control" id="msisdn"
									placeholder="Enter Msisdn" name="msisdn">
							</div>
							<div class="form-group">
								<label for="pwd" style="float: left">Message :</label> <input
									type="text" class="form-control" id="message"
									placeholder="Enter Message" name="message">
							</div>
							<input type="button" class="btn btn-success" id="sendsmsbtn"
								value="Submit">
						</form>
					</div>

					<div class="checkbox">
						<label style="float: left"><input type="checkbox"
							name="remember" id="insurance"> 5. Inclusivity Insurance
						</label>
					</div>
					</br>

				</form>
			</div>

		</center>
	</div>
</body>


<script>
	$("#curbiTrans")
			.change(
					function() {

						// window.location = "http://www.google.com";

						if (this.checked) {

							document.getElementById("pushRetry").checked = false;
							document.getElementById("sms").checked = false;
							document.getElementById("sendSms").checked = false;
							document.getElementById("insurance").checked = false;
							document.getElementById("hiddendiv").style.display = "none";

							$
									.ajax({
										url : "http://localhost:8080/MfsInclusivityWeb/transactionData",
										type : "get", //send it through get method
										/*   data: { 
										    ajaxid: 4, 
										    UserID: UserID, 
										    EmailAddress: EmailAddress
										  }, */
										success : function(response) {
											//Do Something
											var finalresponse = JSON
													.stringify(response);
											//document.getElementById('response').innerHTML = finalresponse;

											var w = window.open('about:blank',
													'Inclusivity');
											w.document.write(finalresponse);
											w.document.close();
											//alert(finalresponse);
										},
										error : function(xhr) {
											//Do Something to handle error
											alert("error");
										}
									});
						}
					});

	$("#pushRetry").on('click', function() {

		// window.location = "http://www.google.com";

		if (this.checked) {

			document.getElementById("curbiTrans").checked = false;
			document.getElementById("sms").checked = false;
			document.getElementById("sendSms").checked = false;
			document.getElementById("insurance").checked = false;
			document.getElementById("hiddendiv").style.display = "none";

			$.ajax({
				url : "http://localhost:8080/MfsInclusivityWeb/pushAndRetry",
				type : "get", //send it through get method
				/*   data: { 
				    ajaxid: 4, 
				    UserID: UserID, 
				    EmailAddress: EmailAddress
				  }, */
				success : function(response) {
					//Do Something
					var finalresponse = JSON.stringify(response);
					var w = window.open('about:blank', 'Inclusivity');
					w.document.write(finalresponse);
					w.document.close();
					//alert("suceess");
				},
				error : function(xhr) {
					//Do Something to handle error
					alert("error");
				}
			});
		}
	});

	$("#sms").on('click', function() {
		//alert("inside onclick");
		// window.location = "http://www.google.com";

		if (this.checked) {
			document.getElementById("curbiTrans").checked = false;
			document.getElementById("pushRetry").checked = false;
			document.getElementById("sendSms").checked = false;
			document.getElementById("insurance").checked = false;

			document.getElementById("hiddendiv").style.display = "none";

			$.ajax({
				url : "http://localhost:8080/MfsInclusivityWeb/sendSmsTrigger",
				type : "get", //send it through get method
				/*   data: { 
				    ajaxid: 4, 
				    UserID: UserID, 
				    EmailAddress: EmailAddress
				  }, */
				success : function(response) {
					//Do Something
					//alert(response);
					var finalresponse = JSON.stringify(response);
					var w = window.open('about:blank', 'Inclusivity');
					w.document.write(finalresponse);
					w.document.close();
				},
				error : function(xhr) {
					//Do Something to handle error
					alert("error");
				}
			});
		}
	});

	$("#sendSms").on('click', function() {

		if (this.checked) {

			document.getElementById("curbiTrans").checked = false;
			document.getElementById("pushRetry").checked = false;
			document.getElementById("sms").checked = false;
			document.getElementById("insurance").checked = false;

			document.getElementById("hiddendiv").style.display = "block";

		} else {
			document.getElementById("hiddendiv").style.display = "none";
		}
	});

	$("#sendsmsbtn").on('click', function() {

		document.getElementById("curbiTrans").checked = false;
		document.getElementById("pushRetry").checked = false;
		document.getElementById("sms").checked = false;
		document.getElementById("insurance").checked = false;

		var msisdn = document.getElementById("msisdn").value;
		var message = document.getElementById("message").value;

		if (msisdn == "" || msisdn == null) {
			alert("Please Enter Msisdn");
			return;
		}

		$.ajax({
			url : "http://localhost:8080/MfsInclusivityWeb/sendSms",
			type : "POST", //send it through get method
			contentType : "application/json",
			dataType : "json",

			data : JSON.stringify({
				msisdn : msisdn,
				message : message
			}),
			success : function(response) {
				//Do Something
				var finalresponse = JSON.stringify(response);
				var w = window.open('about:blank', 'Inclusivity');
				w.document.write(finalresponse);
				w.document.close();
				//alert("suceess");
			},
			error : function(xhr) {
				//Do Something to handle error
				alert("error");
			}
		});

	});

	$("#insurance")
			.on(
					'click',
					function() {

						if (this.checked) {

							document.getElementById("curbiTrans").checked = false;
							document.getElementById("pushRetry").checked = false;
							document.getElementById("sms").checked = false;
							document.getElementById("sendSms").checked = false;

							document.getElementById("hiddendiv").style.display = "none";

							window
									.open("http://localhost:8080/MfsInclusivityWeb/UssdServices.jsp");

						}
					});
</script>
</html>