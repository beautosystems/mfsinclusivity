package com.mfs.client.inclusivity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.dto.SMSApiRequestDto;
import com.mfs.client.inclusivity.dto.SmsResponse;
import com.mfs.client.inclusivity.dto.StatusResponseDto;
import com.mfs.client.inclusivity.dto.TransactionDataDto;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.service.ClaimService;
import com.mfs.client.inclusivity.service.CustomerStatusService;
import com.mfs.client.inclusivity.service.LogSmsQueueData;
import com.mfs.client.inclusivity.service.NotifyService;
import com.mfs.client.inclusivity.service.OptInService;
import com.mfs.client.inclusivity.service.OptOutService;
import com.mfs.client.inclusivity.service.PushAndRetryTransactionDataService;
import com.mfs.client.inclusivity.service.RemittanceStatusService;
import com.mfs.client.inclusivity.service.TransactionDataService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class InclusivityController {

	@Autowired
	LogSmsQueueData logSmsQueueData;

	@Autowired
	TransactionDataService transactionDataService;

	@Autowired
	PushAndRetryTransactionDataService pushAndRetryTransactionDataService;

	@Autowired
	NotifyService notifyService;

	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	OptOutService optOutService;

	@Autowired
	OptInService optInService;

	@Autowired
	ClaimService claimService;

	@Autowired
	RemittanceStatusService remittanceStatusService;

	@Autowired
	CustomerStatusService customerStatusService;

	@PostMapping(value = "/sendSms")
	public ResponseStatus sendSMSAPI(@RequestBody SMSApiRequestDto requestDto) {

		ResponseStatus responseStatus = logSmsQueueData.logSmsQueueData(requestDto);
		return responseStatus;
	}

	@RequestMapping(value = "/transactionData")
	public List<TransactionDataDto> transactionDataFromCurbi() {

		List<TransactionDataDto> response = transactionDataService.getCurbiTransaction();
		return response;
	}

	@RequestMapping(value = "/pushAndRetry")
	public String pushAndRetryTrasnsaction() {

		pushAndRetryTransactionDataService.retryTransaction();
		return "Success";
	}

	@RequestMapping(value = "/sendSmsTrigger")
	public List<SmsResponse> smsTrigger() {

		List<SmsResponse> response = notifyService.sendSMS();
		return response;
	}

	@RequestMapping(value = "/auth")
	public AuthorizationResponseDto createAuthToken() {

		AuthorizationResponseDto response = authorizationService.authorizationService();
		return response;

	}

	@GetMapping(value = "/optin/{msisdn}")
	public ResponseStatus optIn(@PathVariable String msisdn) {
		ResponseStatus response = optInService.optIn(msisdn);
		return response;

	}

	@RequestMapping(value = "/optout/{msisdn}")
	public ResponseStatus optOut(@PathVariable String msisdn) {

		ResponseStatus response = optOutService.optOut(msisdn);

		return response;
	}

	@RequestMapping(value = "/claim/{msisdn}")
	public ResponseStatus claimService(@PathVariable String msisdn) {

		ResponseStatus response = claimService.claim(msisdn);

		return response;

	}

	@GetMapping(value = "/remittancestatus/{msisdn}")
	public StatusResponseDto remittanceStatus(@PathVariable String msisdn) {
		StatusResponseDto response = remittanceStatusService.remittanceStatus(msisdn);
		return response;

	}

	@GetMapping(value = "/customerstatus/{msisdn}")
	public StatusResponseDto customerStatus(@PathVariable String msisdn) {
		StatusResponseDto response = customerStatusService.getStatus(msisdn);
		return response;

	}

}
